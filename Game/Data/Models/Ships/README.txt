Ships are defined in this folder. Each ship is in a subfolder
that contains:
ship.blend = a blend file containing the model. (All texture data should be packed)
icon.jpg = a thumbnail used to show off the ship
data.json = a file containing performance data, ship name etc.
