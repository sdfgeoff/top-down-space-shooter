'''This is the core of an AI and handles it's interface to the ships.
The actual algorithms for the AI can be found in the ai package'''
import random

import bge
import mathutils
import defs
import data

import ai

from . import ship
from . import hud
from . import score


class AI(object):
    '''An AI controlled ship. Note that this does NOT contain the
    actual AI logic, just wraps the ship to isolate AI access. Very
    similar to the player class'''
    def __init__(self, owner, player_id, mode, color):
        self.owner = owner
        self.player_id = player_id

        ship_list = len(data.ship.get_ship_list())

        ship_data = data.ship.get_ship_data(random.randint(0, ship_list))
        self.ship = ship.Ship(
            ship_data,
            self
        )
        self.color = [color['R'], color['G'], color['B'], 1]
        self.ship.hull.color = self.color
        self.score = score.Score()
        self.hud = hud.PlayerHUD(self, self.color, self.ship)

        self.ai_core = ai.get_ai(mode)
        #self.ai_core.shipdata = ship_data

        self.ship.reactor.ondeath.append(self.die)

        self.event = bge.types.GX_Event(self._update)
        bge.logic.globalDict['GAME'].scheduler.register(self.event)

    def die(self, _reactor, _amt):
        '''Blows up the ship'''
        if self.ship.hull.enable:
            self.ship.die()

    def spawn(self):
        '''Spawns a new ship'''
        point = self.owner.level.get_spawn_point()
        self.ship.spawn(point.worldTransform)

    def get_opponents(self):
        '''Returns a list of all 'players' except for this one'''
        opponents = list()
        for opp in self.owner.players:
            if opp is not self:
                if opp.ship.alive:
                    opponents.append(opp)

        return opponents

    def _update(self):
        '''Runs the AI core, filters it's output and sets the various
        ship states to the proper condition'''
        if self.ship.allow_respawn:
            self.spawn()

        if self.ship.alive:
            thrust, turn, gun, missile = self.ai_core.update(
                self.ship.hull.object,
                self.ship.reactor.energy,
                self.get_opponents()
            )
            vec = mathutils.Vector2D([0, thrust, turn])
            if thrust <= defs.SHIP_BACKWARDS_SCALE and gun:
                self.ship.gun.fire()
            if thrust <= defs.SHIP_BACKWARDS_SCALE and missile:
                self.ship.launcher.fire()
            self.ship.engine.output(vec)

    def end(self):
        '''Removes all game-specific data associated with this AI'''
        bge.logic.globalDict['GAME'].scheduler.remove(self.event)
        self.ship.end()
