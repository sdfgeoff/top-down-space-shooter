import bge
import mathutils

import defs

class Engine(object):
    '''An engine that applies thrust to a hull based on a supplied
    2d vector (x thrust, y thrust, turn speed)'''
    def __init__(self, owner, max_force, max_torque, energy_rate):
        self.owner = owner
        self.max_force = max_force
        self.max_torque = max_torque
        self.energy_rate = energy_rate
        
        self._output = mathutils.Vector2D([0,0, 0])
        
        self.event = bge.types.GX_Event(self._update)
        bge.logic.globalDict['GAME'].scheduler.register(self.event)
        
    def output(self, vec):
        '''Set the output. This does not activate the thruster, so 
        it can safely be called as many times per frame as you would
        like'''
        vec[0] = mathutils.clamp(vec[0], -1, 1)
        vec[1] = mathutils.clamp(vec[1], -1, 1)
        vec[2] = mathutils.clamp(vec[2], -1, 1)
        self._output = vec
        
    def _update(self):
        '''Make the thruster apply thrust'''
        obj = self.owner.hull.object
        if not self.owner.hull.enable:
            return
        t = abs(self.event.time_since_last_run)
        # Thrust
        force = self._output.linear
        
        def get_hyper_amt(speed):
            gradient = (1 / (1-defs.SHIP_BACKWARDS_SCALE))
            intercept = 1 - gradient
            hyper = intercept + gradient * speed
            return max(hyper, 0)
            
        def get_req_energy(speed, energy_rate, time):
            regen = defs.ENERGY_REGEN_RATE
            
            if speed < defs.SHIP_BACKWARDS_SCALE + 0.05:  # FP precision
                # Up to SHIP_BACKWARDS_SCALE, it doesn't
                # consume any energy, but instead consumes the up to 
                # the regen
                percent = speed / defs.SHIP_BACKWARDS_SCALE
                consumed = percent * regen
            else:
                percent = min(1, speed / defs.SHIP_BACKWARDS_SCALE)
                gradient = (1) / (1 - defs.SHIP_BACKWARDS_SCALE)
                intercept = 1 - gradient
                engine_use = intercept + gradient * speed
                consumed  = engine_use * energy_rate + regen * percent
            return consumed * time
        
        hyper = get_hyper_amt(force.length)
        time = abs(self.event.time_since_last_run)
        req_energy = get_req_energy(force.length,  self.energy_rate, time)
        
        force.length *= self.max_force
        torque = self._output.turn
        torque *= self.max_torque
        if self.owner.reactor.get_energy(req_energy):
            obj.applyForce(force, True)
        obj.applyTorque([0,0,torque], False)
        
        #Banking
        bank = obj.localOrientation.to_euler()
        bank.y = mathutils.mix(
            -self._output.turn * defs.BANK_ANGLE, 
            bank.y, 
            defs.BANK_SMOOTHING
        )
        bank.x = 0
        obj.localOrientation = bank

        scaling = self._output.linear.length
        target_scale = obj.localScale.copy()
        target_scale.y = 1 + scaling * defs.FORWARD_STRETCH_AMT * hyper
        target_scale.x = 1 + scaling * defs.SIDEWAYS_STRETCH_AMT * hyper
        obj.localScale = obj.localScale.lerp(target_scale, defs.STRETCH_SMOOTHING)
                
        
        # Drag
        lin_vel = obj.worldLinearVelocity
        ang_vel = obj.worldAngularVelocity
        obj.applyForce(lin_vel * -defs.SHIP_DRAG_LINEAR, False)
        obj.applyTorque(ang_vel * -defs.SHIP_DRAG_ANGULAR, False)
        
        obj.worldPosition.z = 0

    def end(self):
        bge.logic.globalDict['GAME'].scheduler.remove(self.event)
