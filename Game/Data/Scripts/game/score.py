class Score(object):
    '''A container for score that includes a callback for when things
    change'''
    def __init__(self):
        self._kills = 0
        self._deaths = 0
        self.onchange = list()
        
    @property
    def kills(self):
        return self._kills
        
    @kills.setter
    def kills(self, val):
        self._kills = val
        for funct in self.onchange:
            funct(self)
            
    @property
    def deaths(self):
        return self._deaths
        
    @deaths.setter
    def deaths(self, val):
        self._deaths = val
        for funct in self.onchange:
            funct(self)
        
    
