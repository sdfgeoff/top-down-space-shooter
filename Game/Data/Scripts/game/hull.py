import bge

import parent
import defs
import loader
import fx
import audio

MISSING_SHIP_ERROR = '''Unable to find ship object {} in blend {}.
Check it is in INACTIVE layer'''

class Hull(object):
    '''All the blender objects are collated in this class. They are
    dynamically loaded from the blenddata'''
    def __init__(self, owner, ship_type):
        self.owner = owner
        self.ship_type = ship_type
        self.object, self.all_objects = load_ship(ship_type)
        col_id = self.owner.owner.player_id
        self._color = [0, 0, 0, 0]
        self.object.collisionGroup = defs.COL_GROUP_SHIPS[col_id]
        self.object.collisionMask = defs.COL_GROUP_ALL_WEAPONS + defs.COL_GROUP_LEVEL - defs.COL_GROUP_WEAPONS[col_id]
        self.object.collisionCallbacks.append(self._hit)
        self.object['SHIP'] = self.owner
        self.enable = False

        self.event = bge.types.GX_Event(self.update)
        bge.logic.globalDict['GAME'].scheduler.register(self.event)

        for launcher in self.get_launchers():
            launcher['OWNER'] = self.owner
        for gun in self.get_guns():
            gun['OWNER'] = self.owner
        for trail in self.get_trails():
            trail['TRAIL'] = fx.mesh.Trail(trail, bge.logic.globalDict['GAME'].scheduler)
            trail['TRAIL'].length = defs.TRAIL_LENGTH

    def end(self):
        '''Free's data associated with the HULL'''
        bge.logic.globalDict['GAME'].scheduler.remove(self.event)
        for trail in self.get_trails():
            trail['TRAIL'].end()
        for obj in self.all_objects:
            obj.endObject()
        self.object.endObject()

    def update(self):
        self.object.color *= 0.8

    def get_launchers(self):
        '''Returns the rocket launcher source point'''
        return [o for o in self.all_objects if 'LAUNCHER' in o]

    def get_guns(self):
        '''Returns the gun source point'''
        return [o for o in self.all_objects if 'GUN' in o]

    def get_trails(self):
        '''Returns all the trail objects'''
        return [o for o in self.all_objects if 'TRAIL_LENGTH' in o]

    def _hit(self, hit_obj, _, __):
        '''Runs when the hull is hit by something'''
        if 'DAMAGE' in hit_obj:
            audio.SinglePlay('Hit.wav')
            self.object.color = self.color
            damage = hit_obj['DAMAGE'].damage
            shot_by = hit_obj['DAMAGE'].source
            if not self.owner.reactor.remove_energy(damage):
                if 'OWNER' in shot_by:
                    other_player = shot_by['OWNER'].owner
                    other_player.score.kills += 1
                    self.owner.owner.score.deaths += 1

    @property
    def color(self):
        '''The players color'''
        return self._color

    @color.setter
    def color(self, col):
        '''Sets the players color (in the children as well)'''
        self._color = col
        for child in self.all_objects:
            if 'COL' in child:
                child.color = col

    def set_position(self, world_transform):
        self.object.worldTransform = world_transform
        self.object.localScale = [1, 1, 1]

    @property
    def enable(self):
        return self._enable

    @enable.setter
    def enable(self, val):
        self._enable = val
        if val is True:
            self.object.worldPosition.z = 0
        else:
            self.object.worldPosition.z = defs.CAM_Z_BUFFER * 2


def load_ship(model_data):
    '''Loads the game object for the specified ship type, libloading if
    necessary.'''
    scene = bge.logic.getCurrentScene()
    root_obj_name = model_data['root_obj']

    if scene.objectsInactive.get(root_obj_name) is None:
        blend_path = model_data['blend']
        err = MISSING_SHIP_ERROR.format(
            root_obj_name,
            blend_path
        )
        bge.log.error(err)
        raise Exception(err)
    else:
        root_obj = scene.addObject(root_obj_name)
        real_root = root_obj.groupMembers[0]
        all_objs = list()
        for child in root_obj.childrenRecursive:
            all_objs.append(child)
            if child.groupMembers is not None:
                for mem in child.groupMembers:
                    mem.setParent(child)
                    all_objs.append(mem)
            if child.parent == root_obj:
                parent.Parent(child, real_root)
        return real_root, all_objs
