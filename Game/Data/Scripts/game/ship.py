import os
import json
import bge

import defs

from .engine import Engine
from .weapons import Launcher, Gun
from .reactor import Reactor
from .hull import Hull
from . import explosions

def get_ship_data(ship_name):
    path = bge.logic.path('Data', 'Models', 'Ships', ship_name, defs.SHIP_DATA_FILE)
    if os.path.isfile(path):
        return json.load(open(path))
    bge.log.error("Unable to find ship: {}".format(ship_name))
    return None
    

class Shield(object):
    def __init__(self, owner):
        self.owner = owner


class Ship(object):
    def __init__(self, ship_data, owner):
        self.data = ship_data
        self.owner = owner
        self.hull = Hull(
            self, 
            ship_data['modeldata']
        )
        self.engine = Engine(
            self,
            ship_data['engine']['force'],
            ship_data['engine']['torque'],
            ship_data['engine']['energy_rate'],
        )
        self.launcher = Launcher(
            self,
            ship_data['launcher']['refire_delay'],
            ship_data['launcher']['energy_cost'],
            ship_data['launcher']['missile']
        )
        self.gun = Gun(
            self,
            ship_data['gun']['refire_delay'],
            ship_data['gun']['energy_cost'],
            ship_data['gun']['bullet'],
        )
        self.shield = Shield(self)
        self.reactor = Reactor(self)
        self._alive = False
        self._allow_respawn = True
        
    def spawn(self, world_transform):
        '''Makes the ship appear'''
        self.hull.enable = True
        self.hull.set_position(world_transform)
        self.gun.reset()
        self.launcher.reset()
        self.reactor.reset()
        self._alive = True
        self._allow_respawn = False
        for trail in self.hull.get_trails():
            trail['TRAIL'].reset()
        
    def die(self):
        '''Makes the ship dissapear'''
        self._alive = False
        explosions.PlayerExplosion(self.hull.object)
        
        self.hull.enable = False
        e = bge.types.GX_Event(self._enable_respawn, num_runs=1, time_between=defs.DEATH_TIME)
        bge.logic.globalDict['GAME'].scheduler.register(e)
        for trail in self.hull.get_trails():
            trail['TRAIL'].reset()
        
    def _enable_respawn(self):
        self._allow_respawn = True

    @property
    def alive(self):
        return self._alive
        
    @property
    def allow_respawn(self):
        return self._allow_respawn

    def end(self):
        self.hull.end()
        self.engine.end()
        self.reactor.end()
