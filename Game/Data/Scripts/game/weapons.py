import time
import audio
import defs

from . import projectiles

class Weapon(object):
    def __init__(self, owner, refire_delay, projectile, sources):
        self.refire_delay = refire_delay
        self.projectile = projectile
        self.sources = sources
        self.last_fired = time.time()
        
    def fire(self):
        bullet_list = list()
        if self.can_fire():
            self.last_fired = time.time()
            for source in self.sources:
                bullet_list.append(self.projectile(source))
        return bullet_list

    def reset(self):
        self.last_fired = time.time()
                
            
    def can_fire(self):
        '''Returns True if the weapon can fire (ie isn't reloading)'''
        if time.time() > self.last_fired + self.refire_delay:
            return True
        return False

class Launcher(object):
    def __init__(self, owner, refire_delay, energy_cost, missile_data):
        self.owner = owner
        self.weapon = Weapon(
            self, 
            refire_delay,
            projectiles.Missile(
                self, 
                missile_data['speed'],
                missile_data['timeout'],
                missile_data['damage'],
                missile_data['turn_rate']
            ),
            self.owner.hull.get_launchers()
        )
        self.energy_cost = energy_cost

    def reset(self):
        self.weapon.reset()
    
    def fire(self):
        '''Attempts to fire the weapon'''
        if self.weapon.can_fire():
            if self.owner.reactor.get_energy(self.energy_cost):
                missiles = self.weapon.fire()
                col_group = defs.COL_GROUP_WEAPONS[self.owner.owner.player_id]
                audio.SinglePlay('Missile.wav')
                for missile in missiles:
                    missile.object.collisionGroup = col_group
                    missile.object.collisionMask = defs.COL_GROUP_ALL_SHIPS + defs.COL_GROUP_LEVEL
                    missile.object.color = self.owner.owner.color

        
class Gun(object):
    def __init__(self, owner, refire_delay, energy_cost, bullet_data):
        self.owner = owner
        self.weapon = Weapon(
            self, 
            refire_delay,
            projectiles.Bullet(
                self,
                bullet_data['speed'],
                bullet_data['timeout'],
                bullet_data['damage'],
            ),
            self.owner.hull.get_guns()
        )
        self.energy_cost = energy_cost

    def reset(self):
        self.weapon.reset()
    
    def fire(self):
        '''Attempts to fire the weapon'''
        if self.weapon.can_fire():
            num_guns = len(self.weapon.sources)
            if self.owner.reactor.get_energy(self.energy_cost*num_guns):
                bullets = self.weapon.fire()
                col_group = defs.COL_GROUP_WEAPONS[self.owner.owner.player_id]
                audio.SinglePlay('Laser.wav')
                for bullet in bullets:
                    bullet.object.collisionGroup = col_group
                    bullet.object.collisionMask = defs.COL_GROUP_ALL_SHIPS + defs.COL_GROUP_LEVEL
                    bullet.object.color = self.owner.owner.color
                    bullet.object.mass = self.weapon.projectile.damage * defs.DAMAGE_MASS_RATIO
        



