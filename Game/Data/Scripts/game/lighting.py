import bge

import loader

NUM_SPOT_LIGHTS = 5
SPOT_PATH = bge.logic.path('Data', 'Models', 'Lights', 'spot.blend')


class LightingManager(object):
    def __init__(self, initial_loader):
        self.event = bge.types.GX_Event(self._update)
        self.scene = None
        self.light_dummies = list()
        self.loaded_lights = list()
        self.raw_lights = list()
        
        initial_loader.register_first(self._load_blend, args=[SPOT_PATH, NUM_SPOT_LIGHTS])

    def _load_blend(self, path, num):
        '''Loads a point light'''
        for _ in range(num):
            light_obj = loader.lib_load_unique(path)[1][0]
            self.raw_lights.append(light_obj)
            self.loaded_lights.append(LightSource(self, light_obj))
                

    def start(self, scene):
        '''Runs after the level has been loaded'''
        bge.logic.globalDict['GAME'].scheduler.register(self.event)
        self.scene = scene
        for obj in self.scene.objects:
            if isinstance(obj, bge.types.KX_LightObject):
                if obj not in self.raw_lights:
                    self.light_dummies.append(LightDummy(obj))

    def end(self):
        '''Runs at the end of a round (when the level is no longer
        loaded)'''
        bge.logic.globalDict['GAME'].scheduler.remove(self.event)
        self.light_dummies = list()
        self._update()

    def _get_sorted_light_list(self):
        weighted_list = list()
        for light in self.light_dummies:
            distance = (self.scene.active_camera.worldTransform - light.worldTransform).to_translation().length
            weight = distance
            weighted_list.append((weight, light))
        weighted_list.sort(key=lambda x: x[0])
        return [l[1] for l in weighted_list]
        

    def _update(self):
        light_sorted_list = self._get_sorted_light_list()  # Sort by distance
        for light_id, light in enumerate(self.loaded_lights):
            if light.available:
                best_light = [l for l in light_sorted_list if l.assigned_source is None]
                if best_light:
                    light.assign_to(best_light[0])
            if light.assigned_dummy not in light_sorted_list[:NUM_SPOT_LIGHTS]:
                light.assign_to(None)

            light.update()
                
class LightSource(object):
    def __init__(self, owner, actual_light):
        self.owner = owner
        self.light = actual_light
        self.assigned_dummy = None
        self.available = True

    def assign_to(self, dummy):
        if self.assigned_dummy is not None:
            self.assigned_dummy.assigned_source = None
        self.assigned_dummy = dummy
        if dummy is not None:
            self.available = False
            dummy.assigned_source = self

        if dummy is not None:
            self.assigned_dummy = dummy
            self.light.worldTransform = dummy.worldTransform
            self.light.energy = 0.0
            self.light.distance = dummy.distance
            self.light.color = dummy.color
            self.light.spotsize = dummy.spotsize
            self.light.spotblend = dummy.spotblend
        

    def update(self):
        if self.assigned_dummy is not None:
            self.light.energy = self.light.energy * 0.8 + self.assigned_dummy.energy * 0.2
        else:
            self.light.energy *= 0.8
            if self.light.energy < 0.05:
                self.available = True

class LightDummy(object):
    '''Stores all the stats for where a light should go'''
    def __init__(self, source_light):
        self.type = source_light.type
        self.energy = source_light.energy
        self.distance = source_light.distance
        self.color = source_light.color
        self.worldTransform = source_light.worldTransform
        self.spotblend = source_light.spotblend
        self.spotsize = source_light.spotsize
        source_light.endObject()
        self.assigned_source = None


