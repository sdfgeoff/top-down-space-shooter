import defs
import bge

class Reactor(object):
    def __init__(self, owner):
        self.owner = owner
        self.__energy = defs.ENERGY_INITIAL
        self.max_energy = defs.ENERGY_INITIAL
        self.regen_rate = defs.ENERGY_REGEN_RATE
        self.ondeath = list()
        self.onchange = list()
        
        self.regen_event = bge.types.GX_Event(self._regen)
        bge.logic.globalDict['GAME'].scheduler.register(self.regen_event)
        
    def _regen(self):
        '''Puts energy into the energy bank'''
        time = abs(self.regen_event.time_since_last_run)
        if self._energy < self.max_energy and self.energy > 0:
            self._energy = min(self._energy + self.regen_rate * time, self.max_energy)
            
    def reset(self):
        self._energy = defs.ENERGY_INITIAL
        
    @property
    def energy(self):
        return self._energy
        
    @property
    def _energy(self):
        return self.__energy
    
    @_energy.setter
    def _energy(self, amt):
        self.__energy = amt
        if self._energy < 0 and self.owner.alive:
            for funct in self.ondeath:
                funct(self, amt)
        for funct in self.onchange:
            funct(self, amt)
        
    def end(self):
        '''Deletes reactor and it's tasks'''
        bge.logic.globalDict['GAME'].scheduler.remove(self.regen_event)
        
    def get_energy(self, amt):
        if self._energy > amt:
            self._energy -= amt
            return True
        else:
            return False
            
    def remove_energy(self, amt):
        self._energy -= amt
        if self._energy > 0:
            return True
        return False
        
            
