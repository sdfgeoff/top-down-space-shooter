import bge
import mathutils

from . import ship
from . import hud
from . import score
import events
import defs


class Player(object):
    def __init__(self, owner, player_id, shiptype, color, controls):
        self.owner = owner
        self.player_id = player_id
        ship_data = ship.get_ship_data(shiptype)
        self.ship = ship.Ship(
            ship_data,
            self
        )
        self.color = [color['R'], color['G'], color['B'], 1]
        self.ship.hull.color = self.color
        self.controls = events.load_config_dict('player{}'.format(player_id), controls)
        self.score = score.Score()
        self.hud = hud.PlayerHUD(self, self.color, self.ship)
        
        self.ship.reactor.ondeath.append(self.die)
        
        self.event = bge.types.GX_Event(self._update)
        bge.logic.globalDict['GAME'].scheduler.register(self.event)
        
        
    def die(self, reactor, amt):
        if self.ship.hull.enable:
            self.ship.die()
            
    
    def spawn(self):
        '''Spawns the player somewhere'''
        point = self.owner.level.get_spawn_point()
        self.ship.spawn(point.worldTransform)
        
        
    def _update(self):
        '''Get player input and move the ship'''
        vec = mathutils.Vector2D([0, 0, 0])
        inp = self.controls
        speed = inp['forwards'].value + inp['backwards'].value * defs.SHIP_BACKWARDS_SCALE
        speed = max(0.0, min(speed, 1.0))
        vec.y = speed
        vec.turn = inp['left'].value - inp['right'].value
        
        if self.ship.allow_respawn:
            self.spawn()
        
        if speed <= defs.SHIP_BACKWARDS_SCALE + 0.01:
            if inp['gun'].value > 0.5:
                self.ship.gun.fire()
            if inp['launcher'].value > 0.5:
                self.ship.launcher.fire()
        
        self.ship.engine.output(vec)
        
    def end(self):
        bge.logic.globalDict['GAME'].scheduler.remove(self.event)
        self.ship.end()
