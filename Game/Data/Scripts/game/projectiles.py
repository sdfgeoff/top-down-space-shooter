import defs
import bge

from . import explosions

class Projectile(object):
    def __init__(self, source, speed, timeout, damage, obj_name, explosion=None):
        '''Creates an instance of an object'''
        self.speed = speed
        self.timeout = timeout
        self.damage = damage
        self.obj_name = obj_name
        self.explosion = explosion
        self.source = source
        self.spawn(source)
        
    def spawn(self, source):
        self.object = source.scene.addObject(self.obj_name, source)
        self.object.localLinearVelocity.z = self.speed
        self.object.collisionCallbacks.append(self.end)
        self.object['DAMAGE'] = self
        self.event = bge.types.GX_Event(self.end, time_between=self.timeout)
        bge.logic.globalDict['GAME'].scheduler.register(self.event)
        
    def end(self, *args):
        if self.event in bge.logic.globalDict['GAME'].scheduler.events:
            if args and self.explosion is not None:
                self.explosion(self.object)
            self.object.endObject()
            bge.logic.globalDict['GAME'].scheduler.remove(self.event)
            
class Homing(Projectile):
    def __init__(self, source, speed, timeout, damage, obj_name, turn_rate, explosion=None): 
        self.speed = speed
        self.timeout = timeout
        self.damage = damage
        self.explosion_damage = damage/10
        self.obj_name = obj_name
        self.explosion = explosion
        self.turn_rate = turn_rate
        self.source = source
        self.spawn(source)
        self.ai_event = bge.types.GX_Event(self.ai)
        bge.logic.globalDict['GAME'].scheduler.register(self.ai_event)
        
    def ai(self):
        if self.source.invalid:
            self.end()
            return
        fired_player = self.source['OWNER'].owner.player_id
        targets = list()
        for obj in self.source.scene.objects:
             if 'SHIP' in obj and obj['SHIP'].alive:
                 if obj['SHIP'].owner.player_id is not fired_player:
                     targets.append(obj)
        self.home_to_target(self.pick_target(targets))
    
    def pick_target(self, targets):
        min_score = 9999
        min_target = None
        
        fired_group = self.source['OWNER'].owner.ship.hull.object.collisionGroup
        col_mask = defs.COL_GROUP_ALL_SHIPS - fired_group + defs.COL_GROUP_LEVEL
        
        for target in targets:
            if self.object.rayCast(target, self.object, 30, '', 0, 0, 0, col_mask)[0] != target:
                continue
            target_pos = target.worldPosition
            missile_pos = self.object.worldPosition
            missile_vect = self.object.getAxisVect([0,0,1])
            tar_vect = (target_pos - missile_pos)
            dist = tar_vect.length
            signed_angle = missile_vect.xy.angle_signed(tar_vect.xy)
            req_turn_radius = abs(signed_angle) * dist  # Approximation
            turn_cost = req_turn_radius / self.turn_rate
            score = turn_cost + dist
            if score < min_score:
                min_score = score
                min_target = target
                
        return min_target
        
    def home_to_target(self, target):
        self.object.localLinearVelocity = [0,0,self.speed]
        if target is not None:
            target_pos = target.worldPosition
            missile_pos = self.object.worldPosition
            missile_vect = self.object.getAxisVect([0,0,1])
            tar_vect = (target_pos - missile_pos)
            diff = missile_vect.cross(tar_vect)
            diff.length = min(diff.length*100, self.turn_rate*30)
            self.object.worldAngularVelocity = diff
        else:
            self.object.worldAngularVelocity = [0,0,0]
        
    def end(self, *args):
        if self.event in bge.logic.globalDict['GAME'].scheduler.events:
            if args and self.explosion is not None:
                self.explosion(self.object, self.explosion_damage)
            self.object.endObject()
            bge.logic.globalDict['GAME'].scheduler.remove(self.event)
            bge.logic.globalDict['GAME'].scheduler.remove(self.ai_event)


class Bullet(object):
    def __init__(self, owner, speed, timeout, damage):
        self.owner = owner
        self.speed = speed
        self.timeout = timeout
        self.damage = damage
    
    def __call__(self, source):
        return Projectile(
            source, 
            self.speed, 
            self.timeout, 
            self.damage, 
            'BulletObj',
            explosions.BulletExplosion
        )
    
class Missile(object):
    def __init__(self, owner, speed, timeout, damage, turn_rate):
        self.owner = owner
        self.speed = speed
        self.timeout = timeout
        self.turn_rate = turn_rate
        self.damage = damage
    
    def __call__(self, source):
        return Homing(
            source, 
            self.speed, 
            self.timeout, 
            self.damage, 
            'MissileObj', 
            self.turn_rate,
            explosions.MissileExplosion
        )
    
