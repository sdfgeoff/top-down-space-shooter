import bge
import math
import audio

import defs

class Explosion(object):
    '''A general explosion - does damange and applies force at a distance
    Does not apply forces to objects with 'NO_FORCE'

    Damage is only applied to ships at the moment, though this may
    be extended if some arbitary damage system is established
    '''
    def __init__(self, spawn_obj, obj, force, damage, distance, life):
        self.object = spawn_obj.scene.addObject(obj, spawn_obj)
        rand_ori = [bge.logic.getRandomFloat()*math.pi,bge.logic.getRandomFloat()*math.pi,bge.logic.getRandomFloat()*math.pi]
        self.object.worldOrientation = rand_ori
        self.event = bge.types.GX_Event(self.end, num_runs=1, time_between=life)
        bge.logic.globalDict['GAME'].scheduler.register(self.event)
        self.apply_damage(force, damage, distance)
        
    def apply_damage(self, force, damage, max_distance):
        for obj in self.object.scene.objects:
            if obj.mass and max_distance != 0:  #It's a Physics object
                vect = self.object.worldPosition - obj.worldPosition
                actual_distance = (vect).length
                percent_distance = max(0, 1 - (actual_distance / max_distance))
                if 'SHIP' in obj and damage:  #Can do damage
                    done_damage = damage * percent_distance
                    obj['SHIP'].reactor.remove_energy(done_damage)
                if 'NO_FORCE' not in obj:
                    applied_force = vect.copy()
                    applied_force.length = percent_distance * force
                    obj.applyForce(-applied_force, False)

    def end(self):
        self.object.endObject()
        
class MissileExplosion(Explosion):
    def __init__(self, spawn_obj, damage):
        obj = spawn_obj.scene.objectsInactive['MissileExplosion']
        force = 500.0  # force to exert on nearby objects
        distance = 2  # blast radius
        life = 1.0    # seconds before despawns
        audio.SinglePlay('MidExplosion.wav')
        super().__init__(spawn_obj, obj, force, damage, distance, life)
        
class BulletExplosion(Explosion):
    def __init__(self, spawn_obj):
        obj = spawn_obj.scene.objectsInactive['BulletExplosion']
        force = 0  # force to exert on nearby objects
        distance = 0  # blast radius
        life = 0.5    # seconds before despawns
        damage = defs.PLAYER_EXPLOSION_DAMAGE
        audio.SinglePlay('TinyExplosion.wav')
        super().__init__(spawn_obj, obj, force, damage, distance, life)


class PlayerExplosion(Explosion):
    def __init__(self, spawn_obj):
        obj = spawn_obj.scene.objectsInactive['PlayerExplosion']
        force = 1000  # force to exert on nearby objects
        distance = 3  # blast radius
        life = 2.0    # seconds before despawns
        damage = 0
        audio.SinglePlay('BigExplosion.wav')
        super().__init__(spawn_obj, obj, force, damage, distance, life)
