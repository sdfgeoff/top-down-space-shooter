import os
import bge


import fx

class PlayerHUD(object):
    def __init__(self, owner, color, ship):
        self.owner = owner
        self.color = color
        self.ship = ship

        sce = [s for s in bge.logic.getSceneList() if s.name == 'HUD'][0]
        huds = [o for o in sce.objects if 'HUD' in o]
        for hud in huds:
            if hud['HUD'] == self.owner.player_id:
                self.hud = hud
                break
        self.hud.color = self.color
        for child in self.hud.children:
            child.color = self.color
            if 'ENERGY' in child:
                self.energy_bar = child
            if 'PLAYER_TEXT' in child:
                self.text_obj = child
        self.ship.reactor.onchange.append(self._update_energy)
        self.ship.owner.score.onchange.append(self._update_kills)
        self.show_picture(self.ship.data['icon'])

    def _update_energy(self, reactor, val):
        self.energy_bar['ENERGY'] = val

    def _update_kills(self, score):
        self.text_obj.text = "{:02d} | {:02d}".format(score.kills, score.deaths)

    def show_picture(self, image_file):
        full_path = bge.logic.path('Data', 'Models', 'Ships', image_file)
        if not os.path.isfile(full_path):
            bge.log.warn("Unable to find ship image at {}. Using default".format(full_path))
            full_path = bge.logic.path('Data', 'Textures', 'noship.png')
        self.tex = fx.tex.show_picture(self.hud, full_path)

class GameHUD(object):
    def __init__(self):
        bge.logic.addScene('HUD')

    def init_hud(self):
        self.scene = [s for s in bge.logic.getSceneList() if s.name == 'HUD'][0]
        self.big_text = self.scene.objects['BigText']
        self.big_text.text = ''
        self.set_visible(False)

    def set_visible(self, val):
        for o in self.scene.objects:
            o.visible = val
