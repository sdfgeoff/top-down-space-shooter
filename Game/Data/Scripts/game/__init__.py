'''The game package deals with the everything to do with the actual
game. This includes scores, shooting etc. etc.

This module and the class within deal with the loading and running of
the game'''
import bge
import defs

import loader
import events
import audio
import data

from . import camera
from . import player
from . import level
from . import ai
from . import hud
from . import lighting



def init(cont):
    '''Create the game'''
    if not cont.sensors[0].positive:
        return
    if 'MENU' not in bge.logic.globalDict:
        load_screen = loader.LoadScreen()
    else:
        load_screen = bge.logic.globalDict['MENU'].load_screen

    initial_loader = GameLoader(load_screen)
    cont.owner['loader'] = initial_loader
    cont.owner['game'] = Game(cont.owner.scene, initial_loader)
    cont.script = __name__ + '.load'


def load(cont):
    '''Load initial assets'''
    if not cont.sensors[0].positive:
        return
    if 'MENU' not in bge.logic.globalDict or bge.logic.globalDict['MENU'].loaded:
        if cont.owner['loader'].update():
            if 'MENU' not in bge.logic.globalDict:
                gamedata = defs.DEFAULT_GAME
                cont.owner['game'].start_game(gamedata)
            cont.script = __name__ + '.run'

def run(cont):
    '''Update the game'''
    if not cont.sensors[0].positive:
        return
    cont.owner['game'].update()


class Game(object):
    '''The Game object encapsulates the entire game, and is essentially
    the root of it all. It should be updated every frame.'''
    def __init__(self, scene, initial_loader):
        bge.logic.globalDict['GAME'] = self
        self.scene = scene
        self.load_screen = None
        self.game_loader = None
        self.lighting_manager = lighting.LightingManager(initial_loader)

        self.gamedata = None   # \
        self.players = list()  # | - These are specific to the game
        self.level = None      # |
        self.camera = None     # /

        self.hud = None
        self.scheduler = bge.types.GX_Scheduler()
        self.scheduler.profile = defs.PROFILE_LOGIC
        self.event = bge.types.GX_Event(self.load)
        self.update_input_event = bge.types.GX_Event(events.manager.update)

        initial_loader.register(self.create_hud)
        initial_loader.register(self.load_hud)

    def start_game(self, gamedata):
        '''Actually starts the game with the specified gamedata'''
        if self.gamedata is not None:
            return  # Don't start two games at the same time
        self.gamedata = gamedata

        # Create the load screen and load system
        self.load_screen = loader.LoadScreen()
        self.game_loader = loader.Loader()
        self.load_screen.add_loader(self.game_loader)

        self.game_loader.register(self.load_camera)
        self.game_loader.register(self.hud.set_visible, args=[True])
        self.game_loader.register(self.load_level)
        for player_id in range(len(gamedata['players'])):
            self.game_loader.register(self.add_player, args=[player_id])
        self.scheduler.register(self.event)
        self.scheduler.register(self.update_input_event)

    def end_game(self):
        '''Ends the current game'''
        if self.gamedata is None:  # Don't bother to remove an already empty game
            return
        self.gamedata = None
        if self.level is not None:
            self.level.end()
            self.level = None
        if self.players is not None:
            for entity in self.players:
                entity.end()
            self.players = list()
        if self.camera is not None:
            self.camera.end()
            self.camera = None
        self.hud.set_visible(False)
        self.lighting_manager.end()
        self.scheduler.remove(self.update_input_event)

        # Force kill all remaining events
        bge.log.info("Removing {} events".format(len(self.scheduler.events)))
        for event in self.scheduler.events.copy():
            bge.log.info("Running {}".format(event))
            event(self.scheduler)
        if self.scheduler.events:
            bge.log.error("Force Removing {} events".format(len(self.scheduler.events)))
            for event in self.scheduler.events.copy():
                bge.log.error("Force Removing {}".format(event))
                self.scheduler.remove(event)

        if defs.PROFILE_LOGIC:
            self.scheduler.print_stats()
        if 'MENU' in bge.logic.globalDict:
            bge.logic.globalDict['MENU'].active = True

    def load_level(self):
        '''Loads the game level'''
        level_name = self.gamedata['level']
        bge.log.info("Loading Level {}".format(level_name))
        self.level = level.Level(level_name)
        self.lighting_manager.start(self.scene)

    def load_camera(self):
        '''Sets up the games camera'''
        self.camera = camera.Camera(self.scene.active_camera)

    def create_hud(self):
        '''Creates the game HUD'''
        self.hud = hud.GameHUD()

    def load_hud(self):
        '''Loads the game HUD'''
        self.hud.init_hud()

    def add_player(self, player_id):
        '''Adds a player/AI into the game'''
        player_data = self.gamedata['players'][player_id]
        if player_data['mode'] == defs.PLAYER_MODE_PLAYER:
            self.players.append(player.Player(
                self,
                player_id,
                player_data['ship'],
                player_data['color'],
                player_data['controls']
            ))
        elif player_data['mode'] != defs.PLAYER_MODE_NONE:
            self.players.append(ai.AI(
                self,
                player_id,
                player_data['mode'],
                player_data['color']
            ))

    def load(self):
        '''Loads the things that can only be loaded when the exact
        specifics of the game are known'''
        if self.game_loader is not None and self.game_loader.update():
            self.scheduler.remove(self.event)

    def update(self):
        '''Updates the whole game'''
        self.scheduler.update()
        if bge.events.ESCKEY in bge.logic.keyboard.active_events:
            self.end_game()




class GameLoader(object):
    '''This handles all initial loading (ie data constant between all
    playthroughts'''
    def __init__(self, load_screen):
        self.load_screen = load_screen
        self.initial_loader = loader.Loader()
        self.register = self.initial_loader.register
        self.load_screen.add_loader(self.initial_loader)
        self.initial_loader.register(audio.init)
        self.done = False

        for blend_id in range(len(defs.BLENDS_TO_LOAD)):
            file_path = defs.BLENDS_TO_LOAD[blend_id]
            path = bge.logic.path('Data', file_path)
            self.initial_loader.register(loader.lib_load_scene, args=[path])

        for ship_id in range(len(data.ship.get_ship_list())):
            ship_data = data.ship.get_ship_data(ship_id)
            ship_path = ship_data['modeldata']['blend']
            ship_path = bge.logic.path('Data', 'Models', 'Ships', ship_path)
            self.initial_loader.register(loader.lib_load_scene, args=[ship_path])

    def register_first(self, function, args=None):
        '''Forces a function to register at the start'''
        if args is None:
            args = list()
        self.initial_loader.functs = [(function, args)] + self.initial_loader.functs

    def update(self):
        '''Updates the loader'''
        # We need to give the system an extra frame to
        # remove the load screen when the loader is done
        # so we return False an extra time here
        prev_done = self.done
        if not self.done:
            loader_done = self.initial_loader.update()
            if loader_done is True:
                self.done = True
        return prev_done
