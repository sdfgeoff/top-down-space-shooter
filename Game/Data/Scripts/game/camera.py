'''The camera needs to view all the player ships, and to do this
it must pan and zoom. This file contains a class that does exactly
that.

Note that it uses some global parameters from defs, and sets the 
camera lens to 60. This may be changed in the future to make
it more general purpose'''

import bge
import mathutils

import defs

# Get the aspect ratio of the window so that a conersion from 
# screen percent to blender units can be done
ASPECT = bge.render.getWindowWidth() / bge.render.getWindowHeight()


class Camera(object):
    '''This is a top-down camera that zooms and pans to look at all
    player objects'''
    def __init__(self, cam):
        self.scene = cam.scene
        self.cam = cam
        self.cam.fov = 60
        self.event = bge.types.GX_Event(self.update)
        bge.logic.globalDict['GAME'].scheduler.register(self.event)
        
    def update(self):
        '''Moves the camera to be overlooking all the player ships'''
        # Generate a list of the active players:
        targets = list()
        for o in self.scene.objects:
            if 'SHIP' in o:
                if o['SHIP'].alive:
                    pos_soon = o.worldPosition + o.worldLinearVelocity * defs.CAM_PREDICT
                    targets.append(pos_soon)
        if not targets:
            return
            
        # Compute bounding box
        min_pos = targets[0].copy()
        max_pos = targets[0].copy()
        for tar in targets:
            max_pos.x = max(tar.x, max_pos.x)
            max_pos.y = max(tar.y, max_pos.y)
            min_pos.x = min(tar.x, min_pos.x)
            min_pos.y = min(tar.y, min_pos.y)
        
        # Find midway point between them
        target = min_pos.lerp(max_pos, 0.5)
        
        # Calculate zoom by the difference
        dist = (min_pos - max_pos)
        dist.y *= ASPECT
        dist.x *= defs.CAM_X_BOUNDARY
        dist.y *= defs.CAM_Y_BOUNDARY
        dist_scalar = dist.xy.length
        target.z = max(dist_scalar, defs.CAM_MIN_DIST)
        
        # Slide between current position and target position
        self.cam.worldPosition = target.lerp(self.cam.worldPosition, defs.CAM_SMOOTHING)
        
        # Set clipping of camera to be around the players
        self.cam.near = self.cam.worldPosition.z - defs.CAM_Z_BUFFER
        self.cam.far = self.cam.worldPosition.z + defs.CAM_Z_BUFFER
    
    def end(self):
        bge.logic.globalDict['GAME'].scheduler.remove(self.event)
    
    
