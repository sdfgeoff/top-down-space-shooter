import bge
import json
import random
import defs

class Level(object):
    def __init__(self, level_name):
        data_path = bge.logic.path(
            'Levels',
            level_name, defs.LEVEL_DATA_FILE
        )
        self.level_data = json.load(open(data_path))
        
        self.blend_path = bge.logic.path(
            'Levels', 
            level_name, self.level_data['blend']
        )
        
        bge.log.info("Loading level file: {}".format(self.blend_path))
        bge.logic.LibLoad(self.blend_path, 'Scene', async=False)
        
        
        sce = bge.logic.getCurrentScene()
        self.spawns = [o for o in sce.objects if 'SPAWN' in o]
        self.spawn_count = 0
        self.valid = True
        
        
        
    def get_spawn_point(self):
        self.spawn_count += 1
        if self.spawn_count <= defs.MAX_PLAYERS:
            return self.spawns[self.spawn_count-1]
        return random.choice(self.spawns)


    def end(self):
        if self.valid:
            bge.logic.LibFree(self.blend_path)
            self.valid = False
