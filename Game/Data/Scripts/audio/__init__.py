'''The audio package contains helper classes for working with audio.'''
import bge
import aud

import defs

# Grab the sound-card
DEVICE = aud.device()

SETTINGS = {
    'sound_effect_volume':0.5,

}

def init():
    '''Sounds take time to load the first time they are used, thankfully we
    can preload them'''
    bge.log.info("Loading Sounds")
    for name in defs.PRELOAD_SOUND_LIST:
        create_factory(name)

def create_factory(name):
    '''Looks for known sounds or loads them from disc. Returns the created
    factory or the existing one. Known sounds are stored in this function
    which is not a nice solution, but works'''
    # Get the full path
    path = bge.logic.path('Data', 'Audio', 'FX', name)

    # Make sure that this function has a known dictionary that is preserved
    # against refresh. THIS IS QUITE HACKY - DON'T DO IT IN YOUR OWN THINGS
    create_factory.known = create_factory.__dict__.get('known', dict())

    if name not in create_factory.known:
        # If we don't already have the sound, load it from disc and buffer it
        # into ram.
        bge.log.info("Loading Sound {}".format(path))
        fac = aud.Factory(path)
        fac.buffer()

        # Store the factory inside this function
        create_factory.known[name] = fac
    else:
        # If we already know the sound, then just return it
        fac = create_factory.known[name]
    return fac


class LoopedSound(object):
    '''Plays a sound infinitely'''
    def __init__(self, path):
        self._factory = create_factory(path).loop(-1)
        self.handle = DEVICE.play(self._factory)
        self.volume = 1.0

    def pause(self):
        '''Pauses the sound'''
        self.handle.pause()

    def resume(self):
        '''Resumes the sound'''
        self.handle.resume()

    @property
    def volume(self):
        '''The volume of the effect - abstracted away from the global effect
        volume. ie if the effect volume is 0.5 and you set this parameter to
        0.6, the sound will be at actual volume 0.3.'''
        return self.handle.volume / SETTINGS['sound_effect_volume']

    @volume.setter
    def volume(self, val):
        self.handle.volume = val * SETTINGS['sound_effect_volume']

    @property
    def pitch(self):
        '''The pitch of the sound'''
        return self.handle.pitch

    @pitch.setter
    def pitch(self, val):
        self.handle.pitch = val


class SinglePlay(object):
    '''Plays a sound once'''
    def __init__(self, path):
        self._factory = create_factory(path)
        self.handle = DEVICE.play(self._factory)
        self.volume = 1.0
        self.pitch = self.handle.pitch

    def pause(self):
        '''Pauses the sound'''
        self.handle.pause()

    def resume(self):
        '''Resumes the sound'''
        self.handle.resume()

    @property
    def volume(self):
        '''The volume of the effect - abstracted away from the global effect
        volume. ie if the effect volume is 0.5 and you set this parameter to
        0.6, the sound will be at actual volume 0.3.'''
        return self.handle.volume / SETTINGS['sound_effect_volume']

    @volume.setter
    def volume(self, val):
        self.handle.volume = val * SETTINGS['sound_effect_volume']

    @property
    def pitch(self):
        '''The pitch of the sound'''
        return self.handle.pitch

    @pitch.setter
    def pitch(self, val):
        self.handle.pitch = val
