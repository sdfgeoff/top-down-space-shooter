'''The sources package defines several modules to help have transparent
input handling.

All sources provide an update method that returns the current value of
the source.

'''

import bge

from . import sources
from . import manager
from . import inputfilter

GROUPS = dict()

def get_active_sources():
    '''All active Sources'''
    active_source = list()
    for e_name in bge.events.__dict__:
        e_code = bge.events.__dict__[e_name]
        if e_code in bge.logic.keyboard.active_events:
            source = sources.KeyButtonSource(e_name)
            active_source.append(source)
        if e_code in bge.logic.mouse.active_events:
            if e_name not in ['MOUSEX', 'MOUSEY']:
                source = sources.MouseButtonSource(e_name)
                active_source.append(source)

    for joy_id, joystick in enumerate(bge.logic.joysticks):
        if joystick is None:
            continue
        for button in joystick.activeButtons:
            source = sources.JoystickButtonSource(joy_id, button)
            active_source.append(source)
        for a_id, axis in enumerate(joystick.axisValues):
            if abs(axis) > 0.5:
                source = sources.JoystickAxisSource(joy_id, a_id)
                active_source.append(source)

    return active_source


def get_group_outputs(group_name):
    '''Returns a dict of all Sources'''
    return {e:GROUPS[group_name][e].value for e in GROUPS[group_name]}


def bind(action_name, source, group_name):
    '''Associates an Source with a group name. Applies and
    returns the input filter'''
    pass

def unbind(action_name, source, group_name):
    '''Removes a source from a group'''


def get_config_dict(group_name):
    '''Returns a config dict for an entire group'''
    pass


def load_config_dict(group_name, data_dict):
    '''Creates a group from a data dictionary (Created with
    get_config_dict. Overwrites anything in the config dict.

    Input datadict is expected to be of the form:
    {
        'action_name':{
            'source':{'type':'keybutton', 'key':'WKEY'},
            'min_val':-1,
            'max_val':1,
        }, 'action_name2':
        ....
    '''
    GROUPS[group_name] = GROUPS.get(group_name, dict())
    for event in data_dict:
        if isinstance(data_dict[event], inputfilter.InputFilter):
            event_parsed = data_dict[event]
        else:
            event_parsed = inputfilter.from_dict(data_dict[event])
            GROUPS[group_name][event] = event_parsed
    return GROUPS[group_name]
