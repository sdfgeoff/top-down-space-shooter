import bge
from . import sources


scheduler = bge.types.GX_Scheduler()

def register(inp):
    scheduler.register(bge.types.GX_Event(inp.update))

def update():
    scheduler.update()
