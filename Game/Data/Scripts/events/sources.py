import bge
import defs

INACTIVE = bge.logic.KX_INPUT_NONE


ABBREVIATIONS = {
    'UPARROWKEY': 'UP',
    'DOWNARROWKEY': 'DOWN',
    'LEFTARROWKEY': 'LEFT',
    'RIGHTARROWKEY': 'RIGHT',
    'CAPSLOCKKEY': 'CAPS',
    'LEFTCTRLKEY': 'L CTRL',
    'RIGHTCTRLKEY': 'R CTRL',
    'LEFTALTKEY': 'L ALT',
    'RIGHTALTKEY': 'R ALT',
    'RIGHTSHIFTKEY': 'R SHIFT',
    'LEFTSHIFTKEY': 'L SHIFT',
    'SPACEKEY': 'SPACE',
    'TABKEY': 'TAB',
    'PAD0': 'PAD 0',
    'PAD1': 'PAD 1',
    'PAD2': 'PAD 2',
    'PAD3': 'PAD 3',
    'PAD4': 'PAD 4',
    'PAD5': 'PAD 5',
    'PAD6': 'PAD 6',
    'PAD7': 'PAD 7',
    'PAD8': 'PAD 8',
    'PAD9': 'PAD 9',
}

class RootSource(object):
    def __init__(self):
        self.threshold = 0.5
        self._value = 0.0
        self.callbacks = list()

    def update(self):
        '''This should be overwritten for each subclass'''

    def short_description(self):
        return 'UNKNOWN'


class KeyButtonSource(RootSource):
    '''Source for keyboard and mouse buttons'''
    def __init__(self, key=None):
        self.key_code = bge.events.__dict__[key]
        self.key = key
        self.inp = bge.logic.keyboard
        super().__init__()

    def update(self):
        if self.inp.events[self.key_code] != INACTIVE:
            return 1.0
        return 0.0

    def short_description(self):
        shorter = self.key
        char = bge.events.EventToCharacter(self.key_code, False)
        if shorter in ABBREVIATIONS:
            shorter = ABBREVIATIONS[shorter]
        elif char != '':
            shorter = char
        return '{}'.format(shorter)

    def __repr__(self):
        return "KeyButtonSource('{}')".format(self.key)

    def to_dict(self):
        return {'type':'keybutton', 'key':self.key}


class MouseButtonSource(RootSource):
    def __init__(self, key_str=None):
        self.key_code = bge.events.__dict__[key_str]
        self.key_str = key_str
        super().__init__()

    def update(self):
        if bge.logic.mouse.events[self.key_code] != INACTIVE:
            return 1.0
        else:
            return 0.0

    def short_description(self):
        return '{}'.format(self.key)

    def __repr__(self):
        return "MouseButtonSource(key_str='{}')".format(self.key_str)

    def to_dict(self):
        return {'type':'mousebutton', 'button':self.key_str}


MOUSE_X_AXIS = 0
MOUSE_Y_AXIS = 1
class MouseAxisSource(RootSource):
    def __init__(self, axis=None):
        '''Axis should be a 2-long list'''
        super().__init__()
        self.axis = axis

    def update(self):
        pos = 0.5 - bge.logic.mouse.position[self.axis]
        if pos == 0.5: # Dud reading until the mouse moves.
            pos = 0
        return pos

    def __repr__(self):
        return "MouseDeltaSource(axis={})".format(
            self.axis,
        )

    def to_dict(self):
        return {'type':'mouseaxis', 'axis':self.axis}


class JoystickButtonSource(RootSource):
    def __init__(self, joystick_id=0, button_id=0):
        super().__init__()
        self.joystick_id = joystick_id
        self.button_id = button_id

    def update(self):
        stick = bge.logic.joysticks[self.joystick_id]
        if stick is not None and \
                self.button_id in stick.activeButtons:
            return 1.0
        else:
            return 0.0

    def __repr__(self):
        return "JoystickButtonSource(joystick_id={}, button_id={})".format(
            self.joystick_id,
            self.button_id,
        )

    def to_dict(self):
        return {'type':'joystickbutton', 'joystick_id':self.joystick_id, 'button_id':self.button_id}



class JoystickAxisSource(RootSource):
    def __init__(self, joystick_id=0, axis_id=0):
        super().__init__()
        self.joystick_id = joystick_id
        self.axis_id = axis_id

    def update(self):
        stick = bge.logic.joysticks[self.joystick_id]
        if stick is not None and self.axis_id < stick.numAxis:
            return stick.axisValues[self.axis_id]
        else:
            return 0.0

    def __repr__(self):
        return "JoystickAxisSource(joystick_id={}, axis_id={})".format(
            self.joystick_id,
            self.axis_id,
        )

    def to_dict(self):
        return {
            'type':'joystickaxis',
            'joystick_id': self.joystick_id,
            'axis_id': self.axis_id,
        }


TYPE_STRINGS = {
    'mouseaxis':MouseAxisSource,
    'keybutton':KeyButtonSource,
    'mousebutton':MouseButtonSource,
    'joystickbutton':JoystickButtonSource,
    'joystickaxis':JoystickAxisSource
}

def from_dict(Source):
    if isinstance(Source, RootSource):
        return Source
    if Source['type'] not in TYPE_STRINGS:
        raise ValueError("Unknown Source type requested")
    else:
        args = {e:Source[e] for e in Source if e != 'type'}
        return TYPE_STRINGS[Source['type']](**args)
