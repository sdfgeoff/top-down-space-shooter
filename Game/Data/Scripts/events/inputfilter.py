import bge
from . import sources
from . import manager


INF = float('inf')

class InputFilter(object):
    '''This class represents the traditional filtering needed on an
    input. It takes as a parameter a source (which must have an update
    method that returns a float), and various constraints such as
    minimum and maximum values, deadzone, sensitivity and
    a threshold for converting the analog signal into a binary button
    press'''
    def __init__(self, source=None, min_val=-INF, max_val=INF, threshold=0.5, sensitivity=1.0, deadzone=0.0, integrate=False, exponential=0.0):
        self.source = source
        self._debounce_state = bge.logic.KX_INPUT_NONE
        self.value = 0.0
        self.threshold = 0.5
        self.min_val = min_val
        self.max_val = max_val
        self.threshold = threshold
        self.sensitivity = sensitivity
        self.deadzone = deadzone
        self.integrate = integrate
        self.exponential = exponential

        manager.register(self)


    def update(self):
        '''Reads a value from the input source, applies all constraints
        (Eg thresholding, constraints etc.)'''
        val = self.source.update()
        if abs(val) < self.deadzone:
            val = 0.0
        val *= self.sensitivity
        val = max(min(val, self.max_val), self.min_val)
        if self.integrate:
            self.value += val
        else:
            self.value = val
        val = (val) * (1 - self.exponential) + (val ** 3) * (self.exponential)
        self.update_state()
        return val

    def update_state(self):
        '''Converts an analog value into a digital signal so that
        detecting button presses is easy'''
        if abs(self.value) > self.threshold:
            if self._debounce_state == bge.logic.KX_INPUT_NONE:
                self._debounce_state = bge.logic.KX_INPUT_JUST_ACTIVATED
            elif self._debounce_state == bge.logic.KX_INPUT_JUST_ACTIVATED:
                self._debounce_state = bge.logic.KX_INPUT_ACTIVE
        else:
            if self._debounce_state == bge.logic.KX_INPUT_ACTIVE:
                self._debounce_state = bge.logic.KX_INPUT_JUST_RELEASED
            elif self._debounce_state == bge.logic.KX_INPUT_JUST_RELEASED:
                self._debounce_state = bge.logic.KX_INPUT_NONE

    def __str__(self):
        return "{}: {}".format(self.source, self.value)

    def __repr__(self):
        return "{}: {}".format(self.source, self.value)

    @property
    def state(self):
        return self._debounce_state


    def to_dict(self):
        return {
            'source':self.source.to_dict(),
            'min_val':self.min_val,
            'max_val':self.max_val,
            'threshold':self.threshold,
            'sensitivity':self.sensitivity,
            'deadzone':self.deadzone,
            'integrate':self.integrate,
            'exponential':self.exponential
        }


def from_dict(source):
    if isinstance(source, InputFilter):
        return source
    source['source'] = sources.from_dict(source['source'])
    args = {e:source[e] for e in source if e != 'type'}
    return InputFilter(**args)
