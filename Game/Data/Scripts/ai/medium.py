import math
import bge
import time
import defs
from . import tools



class OutVec(object):
    def __init__(self, speed=0, angle=0, gun=0, missile=0, enthusiasm=99999):
        self.speed = speed
        self.angle = angle
        self.gun = gun
        self.missile = missile
        self.enthusiasm = enthusiasm

    def __add__(self, inp):
        return OutVec(
            self.speed + inp.speed,
            self.angle + inp.angle,
            self.gun + inp.gun,
            self.missile + inp.missile,
            self.enthusiasm + inp.enthusiasm
        )

    def __mul__(self, val):
        return OutVec(
            self.speed * val,
            self.angle * val,
            self.gun * val,
            self.missile * val,
            self.enthusiasm * val
        )

    def __iter__(self):
        return iter((self.speed, self.angle, self.gun, self.missile))


def combine_vecs(vec1, vec2):
    enthusiasm_diff = vec1.enthusiasm - vec2.enthusiasm
    enthusiasm_diff /= 10
    enthusiasm_diff = max(-0.5, min(0.5, enthusiasm_diff)) + 0.5
    return vec2 * enthusiasm_diff + vec1 * (1 - enthusiasm_diff)

class AI(object):
    def __init__(self):
        bge.log.info("Initing Medium AI")
        self.target = None

    def get_target(self, obj, target_list):
        if self.target is None or not self.target.ship.alive:
            min_score = 99999
            target = None
        else:
            target = self.target
            angle, dist = tools.get_polar(obj, target)
            score = abs(angle)*2 + dist
            if not tools.can_see(obj, target):
                score *= 100
            min_score = score*0.6
        for tar in target_list:
            angle, dist = tools.get_polar(obj, tar)
            score = abs(angle)*2 + dist
            if not tools.can_see(obj, tar):
                score *= 100
            if score < min_score:
                target = tar
                min_score = score
        return target

    def wander(self, obj, energy):
        out = OutVec(0, 0, 0, 0)
        out.speed = defs.SHIP_BACKWARDS_SCALE*(energy/100)
        left = tools.get_distance_at_angle(obj, math.pi/4, True)
        right = tools.get_distance_at_angle(obj, -math.pi/4, True)
        forwards = tools.get_distance_at_angle(obj, 0, True)
        clearance = (forwards + left + right)/3
        diff = (right - left) / (clearance*clearance + 0.1)
        noise = math.sin(time.time()) / (clearance*clearance + 0.1)
        out.angle = diff + noise
        out.angle *= 5
        out.enthusiasm = clearance
        return out

    def attack(self, obj, energy, targets):
        if self.target is not None:
            out = OutVec(0, 0, 0, 0)
            angle, dist = tools.get_polar(obj, self.target)
            out.speed = min(energy/20, 1)
            out.speed *= max(0, dist - 5)+0.2
            out.angle = -angle * 5
            out.gun = max(0, (1-abs(angle)) / (dist+0.01))
            out.missile = min(1, dist / 5)
            if  tools.can_see(obj, self.target):
                out.enthusiasm = dist
            else:
                out.enthusiasm = 99999
            return out
        else:
            return OutVec()

    def chase(self, obj):
        if self.target is not None:
            out = OutVec(0, 0, 0, 0)
            out.speed = defs.SHIP_BACKWARDS_SCALE
            angle, dist = tools.get_polar(obj, self.target)
            out.angle = -out.angle
            out.enthusiasm = 1000/(dist+0.01)
            return out
        return OutVec(0, 0, 0, 0)

    def regen_energy(self, obj, energy, targets):
        min_dist = 9999
        for tar in targets:
            ang, dist = tools.get_polar(obj, tar)
            min_dist = min(min_dist, dist)
        enthusiasm = energy / (min_dist+0.001) * 10
        return OutVec(0, 0, 0, 0, enthusiasm)

    def update(self, obj, energy, targets):
        '''This is the AI core'''
        self.target = self.get_target(obj, targets)
        out = OutVec(0,0,0,0)
        out = combine_vecs(out, self.chase(obj))
        out = combine_vecs(out, self.attack(obj, energy, targets))
        out = combine_vecs(out, self.wander(obj, energy))
        out = combine_vecs(out, self.regen_energy(obj, energy, targets))
        return out
