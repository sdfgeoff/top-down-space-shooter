import random

import defs
from . import dummy
from . import medium

AI_MAP = {
    defs.AI_MODE_VERY_EASY: [dummy.AI],
    defs.AI_MODE_EASY: [dummy.AI],
    defs.AI_MODE_MEDIUM: [medium.AI],
    defs.AI_MODE_HARD: [medium.AI]
}

def get_ai(difficulty):
    return random.choice(AI_MAP[difficulty])()
