import bge

from .tools import get_polar, can_see

class AI(object):
    def __init__(self):
        bge.log.warn("Dummy AI core in {}".format(self))
        self.shipdata = None

    def get_target(self, obj, target_list):
        min_score = 99999
        target = None
        for tar in target_list:
            angle, dist = get_polar(obj, tar)
            if not can_see(obj, tar):
                continue
            score = abs(angle)*2 + dist
            if score < min_score:
                target = tar
                min_score = score
        return target

    def update(self, obj, energy, targets):
        '''This is the AI core'''
        shoot = 0
        target = self.get_target(obj, targets)
        if target is not None:
            angle, dist = get_polar(obj, target)
            if energy > 20:
                if dist > 10:
                    speed = 1
                else:
                    speed = 0.4
                    shoot = 1
            else:
                if dist < 20:
                    angle = -angle
                    speed = 0.4
                else:
                    speed = 0
        else:
            angle = 0
            speed = 0
            
        return speed, -angle, shoot, shoot
