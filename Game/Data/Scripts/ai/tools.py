'''These are various tools that should make writing AI's easier.'''
import math
import bge
import mathutils
import defs

def get_polar(obj, world_pos2):
    '''Returns the angle and distance from an object to a position'''
    if not isinstance(world_pos2, mathutils.Vector):
        world_pos2 = world_pos2.ship.hull.object.worldPosition
    tar_vect = obj.worldPosition - world_pos2
    obj_vect = obj.getAxisVect([0, -1, 0])
    if tar_vect.xy.length > 0:
        angle = obj_vect.xy.angle_signed(tar_vect.xy)
    else:
        return 0, 0
    return angle, tar_vect.length


def can_see(obj, tar):
    '''Returns True if there is a line of site between a game object and
    the target (either a player/AI or a worldposition)'''
    tar_obj = -1
    if not isinstance(tar, mathutils.Vector):
        tar_obj = tar.ship.hull.object
        tar = tar.ship.hull.object.worldPosition

    col_mask = defs.COL_GROUP_ALL_SHIPS - obj.collisionGroup + defs.COL_GROUP_LEVEL
    hit_obj, hit_pos, _ = obj.rayCast(tar, obj, 100, '', 0, 0, 0, col_mask)
    if hit_obj:
        if hit_obj == tar_obj:
            return True
        if (hit_pos - tar).length < 0.1:
            return True
    return False


def cast_ray_at_angle(obj, theta, see_players=False):
    '''Casts a ray at an angle from the object. Can set if it
    see's players/ships or not.
    Angle is in radians'''
    axis = [math.sin(theta), math.cos(theta), 0]
    tar = obj.worldPosition + obj.getAxisVect(axis)
    tar.z = 0
    col_mask = defs.COL_GROUP_LEVEL
    if see_players:
        col_mask += defs.COL_GROUP_ALL_SHIPS - obj.collisionGroup
    hit_obj, hit_pos, hit_nor = obj.rayCast(tar, obj, 100, '', 0, 0, 0, col_mask)
    #if hit_obj is not None:
    #    bge.render.drawLine(obj.worldPosition, tar, [1,1,0])
    #    bge.render.drawLine(obj.worldPosition, hit_pos, [1,0,0])
    return hit_obj, hit_pos, hit_nor

def get_distance_at_angle(obj, theta, see_players=False):
    '''Returns the distance to an object at the specified angle'''
    hit_obj, hit_pos, _ = cast_ray_at_angle(obj, theta, see_players)
    if hit_obj is None:
        return 0
    return (obj.worldPosition - hit_pos).length
