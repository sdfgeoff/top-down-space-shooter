_ROOT_PATH = None
import os
import bge


def path(*args):
    '''
    Returns a path relative to the projects root path. To set the 
    root path use set_path_root. All arguments passed in are joined,
    so you can do things like:
    bge.logic.path('Textures', 'wallpaper.jpg')
    '''
    local_path = os.path.join(*args)
    if _ROOT_PATH is None:
        local_path = os.path.join('//', local_path)
        return bge.logic.expandPath(local_path)
    else:
        return os.path.join(_ROOT_PATH, local_path)
        
def set_path_root(root_search_file):
    '''Sets the projects root path by searching upwards from the current
    blend files. Thus the passed in file should be in the projects root
    directory. It should probably be the main blend file.
    
    If it fails, the root directory is set to the current blend's
    directory and an error is raised
    '''
    global _ROOT_PATH
    blend_path = bge.logic.expandPath('//')
    root_path = blend_path
    fail = False
    while not os.path.isfile(os.path.join(root_path, root_search_file)):
        root_path = os.path.split(root_path)[0]
        if root_path == "/" or len(root_path) < 3:
            fail = True
            root_path = blend_path
            break
    _ROOT_PATH = root_path
    if fail:
        raise ValueError('Unable to find: {}'.format(root_search_file))
        
exit_on_error = False
