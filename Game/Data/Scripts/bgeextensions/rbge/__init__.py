from . import logic
from . import types

log = types.GX_Logger('game.log')
logic.scheduler = types.GX_Scheduler()
