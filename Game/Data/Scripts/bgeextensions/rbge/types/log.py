import bge
import traceback

class GX_Logger(object):
    '''A GX_Logger logs output both to the console and to a file.
    It's instance in bge.log should be used in place of python's inbuild
    print function. Basic usage is:
    bge.log.info("Something that may help debugging")
    bge.log.warn("Something non-fatal. Continuing")
    bge.log.error("Something fatal, the game will likely not work")
    
    If you create your own instance, you can pass in your own path.
    Paths are relative to the project directory as fetched using 
    bge.logic.path.
    '''
    ERROR = 'ERROR: '
    WARN =  'WARN:  '
    INFO =  'INFO:  '
    def __init__(self, path):
        self.path = path
            
    def _out_to_file(self, out_str):
        '''Handles output to the actual file'''
        open(bge.logic.path(self.path), 'a').write(out_str + '\n')
        
    def log_raw(self, in_str):
        '''Writes a string as is to the file and prints it. Handles
        the formatting'''
        stack = traceback.extract_stack()[:-2]
        for entry in stack:
            if hasattr(entry, 'filename'):
                self._out_to_file("{}:{}".format(
                    entry.filename, 
                    entry.lineno
                ))
            else:
                self._out_to_file("{}:{}".format(
                    entry[0], 
                    entry[1]
                ))
        self._out_to_file(in_str + '\n')
        last = stack[-1]
        if hasattr(last, 'filename'):
            print("{}:{} {}".format(last.filename, last.lineno, in_str))
        else:
            print("{}:{} {}".format(last[0], last[1], in_str))
        

    def clear_log(self):
        '''Clears the log file'''
        open(bge.logic.path(self.path), 'w').write('')

    def info(self, in_str):
        '''Log something that is expected behaviour, but may help
        debugging, eg the progression of a loading system'''
        self.log_raw(self.INFO + str(in_str))
        
    def warn(self, in_str):
        '''Log something that is not expected behaviour, but should 
        not affect the running of the game.'''
        self.log_raw(self.WARN + str(in_str))
        
    def error(self, in_str):
        '''Log something that is not expected behaviour and will
        likely cause the game to fail, eg missing shader files'''
        self.log_raw(self.ERROR + str(in_str))
