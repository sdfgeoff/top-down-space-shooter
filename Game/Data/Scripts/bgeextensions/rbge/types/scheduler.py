'''A generic scheduler that can be used to schedule events'''
import time
import cProfile

class GX_Scheduler(object):
    '''A scheduler for running functions. By default one is initilized
    in bge.logic.scheduler, though others can easily be created.
    Note that the scheduler has to be updated manually each frame.

    Basic usage:
        e = bge.types.GX_Event(print, args=['text'])
        bge.logic.scheduler.register(e)
        bge.logic.scheduler.update()
        bge.logic.scheduler.update()
        bge.logic.scheduler.update()
    '''
    def __init__(self):
        self.events = list()
        self.update = self.update_noprofile
        self._profile = False
        self.profile = False
        self._profiler = cProfile.Profile()

    def update_profiled(self):
        '''Runs update_noprofile inside a cProfile profiler'''
        self._profiler.runcall(self.update_noprofile)

    def print_stats(self):
        '''Displays the statistics if profiled'''
        self._profiler.print_stats(1)

    def update_noprofile(self):
        '''Runs an iteration of the scheduler, checking and running
        all events. A single function failing will not cause other
        events to not run. Errors are raised at the end of all
        tasks'''
        errors = list()
        for event in self.events:
            if event.time_until_next_run <= 0:
                try:
                    event(self)
                except Exception as err:
                    errors.append(err)
        for err in errors:
            raise err


    def register(self, event):
        '''Adds an event to the scheduler'''
        self.events.append(event)

    def remove(self, event):
        '''Removes an event from the scheduler'''
        self.events.remove(event)

    @property
    def profile(self):
        '''Sets if the events should be profiled so you can analyze the
        performance of items in this scheduler'''
        return self._profile

    @profile.setter
    def profile(self, val):
        self._profile = val
        if val:
            self.update = self.update_profiled
        else:
            self.update = self.update_noprofile


class GX_Event(object):
    '''An event that can be scheduled or called directly.
    The number of runs, the time between runs and arguments to the
    function can all be specified. By default the function will
    be run forever every frame'''
    def __init__(self, funct, args=None, time_between=0, num_runs=-1):
        self.funct = funct
        if args is None:
            self.args = list()
        else:
            self.args = args
        self.time_between = time_between
        self.num_runs = num_runs
        self.last_run = time.time()
        self.remaining_runs = num_runs

    def __call__(self, scheduler):
        '''Runs the function and resets the timer. Decrements run
        counter'''
        self.funct(*self.args)

        if self.time_between == 0:
            self.last_run = time.time()
        else:
            self.last_run = self.last_run + self.time_between

        if self.remaining_runs > 1:
            self.remaining_runs -= 1
        elif self.remaining_runs == 1:
            scheduler.remove(self)

    @property
    def time_since_last_run(self):
        '''Returns the time since the event was last run.

        If the schedular is not keeping up with this event, this
        may not be accurate, it will be when it was supposed to
        run'''
        return self.last_run - time.time()

    @property
    def time_until_next_run(self):
        '''Returns the (ideal) time until the next execution.

        If the schedular is not keeping up with this event, it may be
        negative'''
        return (self.last_run + self.time_between) - time.time()

    def __str__(self):
        return "Event({}.{})".format(self.funct.__module__,
                                     self.funct.__name__)
