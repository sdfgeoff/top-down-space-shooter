import mathutils

def mix(num1, num2, fac):
    '''
    Interpolates between two numbers eg:
    mix(0, 10, 0.2) = 2
    mix(0, 1, 0.3) = 0.3
    '''
    return num1 * (1.0 - fac) + num2 * fac
    
lerp = mix
    
def clamp(num, min_num, max_num):
    '''Constrains a number between two values'''
    return min(max_num, max(min_num, num))
    
def wrap(num, min_num, max_num):
    '''wraps the number between two values'''
    diff = abs(max_num - min_num)
    return min_num + num % diff


def remap(num, old_min, old_max, new_min, new_max):
    '''Changes a value from one range to another'''
    scale = (new_max - new_min) / (old_max - old_min)
    return (num - old_min) * scale + new_min

class Vector2D(mathutils.Vector):
    @property
    def turn(self):
        return self.z
        
    @turn.setter
    def turn(self, val):
        self.z = val
        
    @property
    def linear(self):
        return mathutils.Vector([self.x, self.y, 0])
