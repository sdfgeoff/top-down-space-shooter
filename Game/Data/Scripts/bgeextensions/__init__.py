from . import rbge
from . import rmathutils


import bge
import mathutils

def update_in_module(source, new):
    '''Recursively adds new functions by merging two modules. It creates
    attributes where necessary, and will not overwrite any pre-existing
    values'''
    attributes = dir(new)
    for attr_name in attributes:
        if not attr_name.startswith('__'):
            attr = new.__dict__[attr_name]
            if attr_name not in source.__dict__:
                source.__dict__[attr_name] = attr
            else:
                if str(type(attr)) == "<class 'module'>":
                    if attr is not source:
                        update_in_module(source.__dict__[attr_name], attr)
    
update_in_module(mathutils, rmathutils)
update_in_module(bge, rbge)


import sys
import traceback

def exception_handler(type_, value, traceb):
    '''Runs whenever there is a python exception'''
    msg = ''.join(traceback.format_exception(type_, value, traceb))
    rbge.log.error(msg)
    if bge.logic.exit_on_error:
        # stop the game engine
        bge.logic.endGame()
    

sys.excepthook = exception_handler
