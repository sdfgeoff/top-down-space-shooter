'''Various functions for dealing with the preperation of a game/scene.
This includes a load-manager as well as functions to simplify and extend
LibLoad use.'''

import os

import bge
import defs

MISSING_BLEND_ERROR = "Unable to find model file at {}"

class LoadScreen(object):
    '''This the a screen that displays the loading results from several
    loaders. This one has a progress spiral on it along with a numerical
    display. The blend "Data/Menus/loadscreen.blend" needs to be linked
    into the blend where you want a loadscreen. Sorry this is a
    limitation of blender'''
    def __init__(self):
        # A loadscreen can be used with multiple instances of the
        # loader class (because libload only works in one scene)
        # All of these are stored in self.loaders. They should be
        # added using the add_loader function though.
        self.loaders = list()

        # Add the load screen. Note that it takes one frame to appear.
        bge.logic.addScene('LoadScreen')

        # Because loading typically takes lots of time, and we want
        # to display all of it we need to stop the game engine
        # detecting long frame-times and dropping render frames to
        # try speed it up.
        bge.logic.setMaxLogicFrame(1)

        # We also need to know if the scene has been removed. This is
        # to prevent trying to remove the loadscreen twice
        self.removed = False

    def add_loader(self, loader):
        '''Registers a loader to the loadscreen so that it will be
        displayed as part of the progress'''
        # Store loader in the list of loaders:
        self.loaders.append(loader)

        # Make changes to the loader affect the loadsceen by
        # making the loader call update on the loadscreen when it
        # changes.
        loader.on_update.append(self._update_scene)
        loader.on_end.append(self._remove_scene)

    def _update_scene(self):
        '''Updates everything in the scene'''
        # Iterate through all the shaders and build up a total steps
        # to do and count how many have already been done. Note that
        # we can't simply add percentages because that would divide the
        # circle evenly between loaders while some may actually take
        # much longer than others.
        total = 0
        done = 0
        for loader in self.loaders:
            total += len(loader.functs)
            done += loader.load_count
        percent = done / total

        # Set the various objects in the loadscreen to the correct value
        # The displayed number is capped to 99 so that it the number
        # of digits never changes
        sce = [s for s in bge.logic.getSceneList() if s.name == 'LoadScreen']
        if sce:
            int_percent = min(99, int(percent * 100))
            sce[0].objects['LoadText'].text = "{:02d}".format(int_percent)
            sce[0].objects['LoadText'].resolution = 10
            sce[0].objects['LoadSpiral'].worldPosition.z = percent

    def _remove_scene(self):
        '''Checks to see if all loaders are loaded and if so, removes
        the loadscreen'''
        # Count number of load steps and how many have been done
        total = 0
        done = 0
        for loader in self.loaders:
            total += len(loader.functs)
            done += loader.load_count

        if total == done and not self.removed:
            # Remove the scene and store that this loadscreen object is
            # no longer valid
            self.removed = True
            sce = [s for s in bge.logic.getSceneList() if s.name == 'LoadScreen']
            if sce:
                sce[0].end()

            # Reset the max logic frames so that the game engine once
            # again can drop render frames without loosing logic time
            bge.logic.setMaxLogicFrame(defs.MAX_LOGIC_FRAMES)


class Loader(object):
    '''Iterates through a list of functions each time the update function is
    called. This is useful to spread out loading over multiple frames'''
    def __init__(self):
        # Create a list to store functions (and their arguments) in.
        # This is a list of tuples [(function_handle, argument_list), (funct...
        self.functs = list()

        # This is used to indicate what function we are up to. Public access
        # is through the propery self.load_count which is read-only
        self._load_count = 0

        # List of callbacks which can be run whenever the load state changes
        # and when all functions are loaded
        self.on_update = list()
        self.on_end = list()

        # Wether the loader is loaded. This is set true when only after the
        # on_end callbacks have been run.
        self.done = False

    def update(self):
        '''Runs the next function associated with the loadscreen. Returns
        True if loading is completed. Note that running the callbacks
        is done the call AFTER the loading is actually completed.'''
        if self.load_count < len(self.functs):
            # If we haven't finished, run the function for this update cycle
            funct = self.functs[self.load_count]
            funct[0](*funct[1])
            self._load_count += 1

            # Run callbacks
            for funct in self.on_update:
                funct()
            return False

        elif self.load_count == len(self.functs) and not self.done:
            # If we've done all of the load steps, run any callbacks and mark
            # the loader as being done
            if not self.done:
                for funct in self.on_end:
                    funct()
            self.done = True
            return True

        else:
            # We're done
            return True

    def register(self, funct, args=None):
        '''Register a function to be run by the loader'''
        # You cannot put lists in the default values for the arguments to a
        # function as they act global across calls - so we use None as the
        # argument and convert it to a list here
        if args is None:
            args = list()

        # Store function and arguments
        self.functs.append((funct, tuple(args)))

    @property
    def load_count(self):
        '''Returns the ID of the current function. Loader is done when this
        is equal to the length of the function list'''
        return self._load_count

    @property
    def percent(self, plus_1=True):
        '''Get how done the loader is as a percent. The plus_1 argument is
        used because sometimes we need to know the percent for when the
        current function is done (ie one frame in advance)'''
        count = self.load_count
        if plus_1:
            count = self.load_count + 1
        return (count) / len(self.functs)


def lib_load_scene(path):
    '''Merges a blend file with the current path.
    NOTE THAT THIS DEPENDS ON THE SCENE THE SCRIPT IS CALLED FROM.

    Checks validity of path and throws an error if it does not exist.'''
    if not os.path.isfile(path):
        # Raise error if the path does not exist
        bge.log.error(MISSING_BLEND_ERROR.format(path))
        raise ValueError(MISSING_BLEND_ERROR.format(path))
    else:
        bge.log.info("Loading Blend {}".format(path))
        try:
            bge.logic.LibLoad(path, 'Scene', load_actions=True)
        except ValueError:
            bge.log.info(bge.logic.LibList())
            bge.log.warn("Blend File likely already open. Seems to be an issue on windows") # @todo


def lib_load_unique(path):
    '''Sometimes you want to have completely seperat objects (eg materials).
    Currently this isn't possible using libnew, so we have to load the whole
    blend file by passing in the blend as a data string (otherwise BGE
    complains about loading it multiple times). Note that this will loase any
    relative paths - so you can't use things like external textures in blends
    loaded with this method.

    Returns the library name and a list of added objects. Note that the list
    of added objects includes those on hidden and visible layers'''
    if not os.path.isfile(path):
        # Raise error if the path does not exist
        bge.log.error(MISSING_BLEND_ERROR.format(path))
        raise ValueError(MISSING_BLEND_ERROR.format(path))
    else:
        bge.log.info("Loading Unique {}".format(path))

        scene = bge.logic.getCurrentScene()

        # Store what objects are already in existance so we can find the new
        # ones
        for obj in scene.objects + scene.objectsInactive:
            obj['LIBLOAD_FOUND'] = True

        # Generates a unique library name by incrementing a number
        # Note that it counts up to 19 and then goes 100, 101....
        libname = 'UNIQUE_LIBLOAD0'
        while libname in bge.logic.LibList():
            libname = libname[:-1] + str(int(libname[-1]) + 1)

        # Read the blend file into a string so that blender doesn't know the
        # path it is loaded from. This means BGE won't complain when we try
        # to load the blend again
        raw_data = open(path, 'rb').read()

        # Load the blend!
        bge.logic.LibLoad(libname, 'Scene', raw_data, load_actions=True)

        # Retrieve all objects not noticed in the previous search.
        added = list()
        for obj in scene.objects  + scene.objectsInactive:
            if 'LIBLOAD_FOUND' not in obj:
                added.append(obj)
                obj['LIBLOAD_FOUND'] = True

        return libname, added
