'''Parenting in blender 2.78a has an issue with scaling, so we have to
fix it the hard way. This will introduce a little latency into object
motion, but it shouldn't be noticable.

The bug has been reported and has a patch, but I am not sure when
it will be available to most people. This bug has been around for
years, so I'll likely keep this in here so that the game can run
on old versions of blender
'''
import bge

class Parent(object):
    '''Joins two objects'''
    def __init__(self, child, parent):
        self.child = child
        self.scene = self.child.scene
        self.parent = parent

        # We need to know how far away it is from it's target position
        self.offset = child.localTransform.copy()

        # Schedule it to run every frame just before the scene is drawn
        self.scene.pre_draw.append(self.update)

    def update(self):
        '''Moves the object ot match it's parent's position. Checks that
        both objects still exist - but does not end either of them'''
        if self.child.invalid and self.parent.invalid:
            # If either object is invalid, remove the update callback.
            # Unfortunately calling scene.pre_draw.remove() causes a segfault
            prev = [f for f in self.scene.pre_draw if f is not self.update]
            self.scene.pre_draw = prev
        else:
            # Update the position
            self.child.worldTransform = self.parent.worldTransform*self.offset
