import bgeextensions
import bge
import settings_file

bge.logic.set_path_root('Splay.blend')
bge.log.clear_log()
bge.logic.exit_on_error = True

PROFILE_LOGIC = True
MAX_LOGIC_FRAMES = 10


# ////////////////////////////// Pickers ////////////////////////////////
LEVEL_TILE_SMOOTH = 0.9   # Damping on tile motion

BAR_ENGINE_SCALAR = 0.8   # How do you compare an engine to a gun to a
BAR_GUN_SCALAR = 0.8      # missile? Used to alter length of stats bars
BAR_LAUNCHER_SCALAR = 1.2  # Also see get_engine_score in playershippicker


# ////////////////////////////// Players ////////////////////////////////
# The max player count it limited by number of collision groups to 6, and by
# HUD to 4
MAX_PLAYERS = 4

DEATH_TIME = 1.5  # TIme in seconds before respawn


# //////////////////////////// Motion Stats ////////////////////////////
# Some motion stats should be universal across all ships, such as the
# drag model and stretching.


SHIP_BACKWARDS_SCALE = 0.4  # Slow down when pressing backwards key

SHIP_DRAG_LINEAR = 3  # Amount of force slowing the vehicle down
SHIP_DRAG_ANGULAR = 3

STRETCH_SMOOTHING = 0.2      # Damping on stretch
FORWARD_STRETCH_AMT = 0.5    # Elongation amount
SIDEWAYS_STRETCH_AMT = -0.3  # Width wise

BANK_ANGLE = 1.0             # Angle in radians to bank
BANK_SMOOTHING = 0.9
TRAIL_LENGTH = 3.0

# //////////////////////////// Camera Settings //////////////////////////

CAM_MIN_DIST = 20    # How close the camera can get
CAM_X_BOUNDARY = 1.2    # Border around players
CAM_Y_BOUNDARY = 1.25    # Border around players
CAM_SMOOTHING = 0.9  # Smoothing the camera's motion in world_space
CAM_Z_BUFFER = 15     # How much of the scene above/below the players
CAM_PREDICT = 0.25    # How much to predict player movement.

# //////////////////////////// Energy Stats ////////////////////////////

ENERGY_INITIAL = 100
ENERGY_REGEN_RATE = 1  # energy per second
PLAYER_EXPLOSION_DAMAGE = 10.0

# Relationship between damage and bullet mass so that more damage = more
# kickback. Increasing this number increases the kickback
DAMAGE_MASS_RATIO = 0.02

# ////////////////////////////// File Data //////////////////////////////

SHIP_DATA_FILE = 'data.json'
LEVEL_DATA_FILE = 'data.json'
BLENDS_TO_LOAD = [
    'Models/Bullet.blend',
    'Models/Missile.blend',
    'Models/MissileExplosion.blend',
    'Models/BulletExplosion.blend',
    'Models/PlayerExplosion.blend',
]

PRELOAD_SOUND_LIST = [
    'TinyExplosion.wav',
    'MidExplosion.wav',
    'BigExplosion.wav',
    'Hit.wav',
    'Laser.wav',
    'Missile.wav',
]

# ////////////////////////// Collision Groups //////////////////////////
COL_GROUP_ALL = (2**16 - 1)
COL_GROUP_LEVEL = 2**0
COL_GROUP_SHIPS = [2**(i+1) for i in range(MAX_PLAYERS)]
COL_GROUP_WEAPONS = [2**(i+1+MAX_PLAYERS) for i in range(MAX_PLAYERS)]
COL_GROUP_ALL_SHIPS = sum(COL_GROUP_SHIPS)
COL_GROUP_ALL_WEAPONS = sum(COL_GROUP_WEAPONS)

# ////////////////////////////// PLAYER MODES ////////////////////////////

PLAYER_MODE_NONE = 0
PLAYER_MODE_PLAYER = 1
PLAYER_MODE_AI = 2
AI_MODE_VERY_EASY = 2
AI_MODE_EASY = 3
AI_MODE_MEDIUM = 4
AI_MODE_HARD = 5

PLAYER_MODES = [
    {'name': 'None', 'grey_out': True, 'icon_overide': 'NoShip.jpg'},
    {'name': None, 'grey_out': False, 'icon_overide': None},
    {'name': 'AI Scum', 'grey_out': True, 'icon_overide': 'AIShip.jpg'},
    {'name': 'AI Pleb', 'grey_out': True, 'icon_overide': 'AIShip.jpg'},
    {'name': 'AI Midd', 'grey_out': True, 'icon_overide': 'AIShip.jpg'},
    {'name': 'AI Boss', 'grey_out': True, 'icon_overide': 'AIShip.jpg'},
]


# //////////////////////////// Default Game //////////////////////////#
# This is the game that is played if there is not data passed in from
# the menu: ie playing Game.blend
# It is also used as a template for the menu to change values from.
DEFAULT_GAME = settings_file.SettingsFile(bge.logic.path('settings.conf'), {
    'level': 'feint',
    'players': [
        {
            'ship': 'ld37',
            'mode': PLAYER_MODE_PLAYER,
            'color': {'R': 0.0, 'G': 0.3, 'B': 1.0},
            'controls': {
                'forwards': {'source': {'type': 'keybutton', 'key': 'WKEY'}},
                'backwards': {'source': {'type': 'keybutton', 'key': 'SKEY'}},
                'left': {'source': {'type': 'keybutton', 'key': 'AKEY'}},
                'right': {'source': {'type': 'keybutton', 'key': 'DKEY'}},
                'gun': {'source': {'type': 'keybutton', 'key': 'ONEKEY'}},
                'launcher': {'source': {'type': 'keybutton', 'key': 'TWOKEY'}}
            }
        },
        {
            'ship': 'stinger',
            'mode': PLAYER_MODE_PLAYER,
            'color': {'R': 0.0, 'G': 0.8, 'B': 0.0},
            'controls': {
                'forwards': {'source': {'type': 'keybutton', 'key': 'UPARROWKEY'}},
                'backwards': {'source': {'type': 'keybutton', 'key': 'DOWNARROWKEY'}},
                'left': {'source': {'type': 'keybutton', 'key': 'LEFTARROWKEY'}},
                'right': {'source': {'type': 'keybutton', 'key': 'RIGHTARROWKEY'}},
                'gun': {'source': {'type': 'keybutton', 'key': 'NINEKEY'}},
                'launcher': {'source': {'type': 'keybutton', 'key': 'ZEROKEY'}}
            }
        },
        {
            'ship': 'ld37',
            'mode': PLAYER_MODE_NONE,
            'color': {'R': 1.0, 'G': 0.3, 'B': 0.0},
            'controls': {
                'forwards': {'source': {'type': 'keybutton', 'key': 'IKEY'}},
                'backwards': {'source': {'type': 'keybutton', 'key': 'KKEY'}},
                'left': {'source': {'type': 'keybutton', 'key': 'JKEY'}},
                'right': {'source': {'type': 'keybutton', 'key': 'LKEY'}},
                'gun': {'source': {'type': 'keybutton', 'key': 'NKEY'}},
                'launcher': {'source': {'type': 'keybutton', 'key': 'MKEY'}}
            }
        },
        {
            'ship': 'ld37',
            'mode': PLAYER_MODE_NONE,
            'color': {'R': 1.0, 'G': 0.0, 'B': 0.8},
            'controls': {
                'forwards': {'source': {'type': 'keybutton', 'key': 'IKEY'}},
                'backwards': {'source': {'type': 'keybutton', 'key': 'KKEY'}},
                'left': {'source': {'type': 'keybutton', 'key': 'JKEY'}},
                'right': {'source': {'type': 'keybutton', 'key': 'LKEY'}},
                'gun': {'source': {'type': 'keybutton', 'key': 'NKEY'}},
                'launcher': {'source': {'type': 'keybutton', 'key': 'MKEY'}}
            }
        },
    ],
    'game':{
        'fullscreen':False,
        'bloom':True,
        'music':True,
        'sound':True,
        'trails':True
    }
})
