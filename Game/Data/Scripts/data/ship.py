'''This module handles parseing ship data files, and various other
actions relating to the ship data'''
import json
import os
import bge
import mathutils

import defs


def get_ship_name(offset):
    '''Returns the ship name at a specified offset'''
    ship_dir = bge.logic.path('Data', 'Models', 'Ships')
    valid_dirs = list()
    for ship in os.listdir(ship_dir):
        dir_path = os.path.join(ship_dir, ship)
        if os.path.isdir(dir_path):
            if os.path.isfile(os.path.join(dir_path, defs.SHIP_DATA_FILE)):
                valid_dirs.append(ship)
    choice = mathutils.wrap(offset, 0, len(valid_dirs))
    return valid_dirs[choice]

def get_ship_list():
    '''Creates a list of all known ships'''
    ship_dir = bge.logic.path('Data', 'Models', 'Ships')
    valid_dirs = list()
    for ship in os.listdir(ship_dir):
        dir_path = os.path.join(ship_dir, ship)
        if os.path.isdir(dir_path):
            if os.path.isfile(os.path.join(dir_path, defs.SHIP_DATA_FILE)):
                valid_dirs.append(dir_path)
    return valid_dirs


def get_ship_data(offset):
    '''Returns the data of the specified ship, does a wrapped
    lookup in get_ship_list so you can pass in offsets infinitely
    large!'''
    ship_path = get_ship_path(offset)
    data_file = open(os.path.join(ship_path, defs.SHIP_DATA_FILE))
    return json.load(data_file)

def get_ship_path(offset):
    '''Returns the path to the specified ship, does a wrapped
    lookup in get_ship_list so you can pass in offsets infinitely
    large!'''
    valid_dirs = get_ship_list()
    choice = mathutils.wrap(offset, 0, len(valid_dirs))
    return valid_dirs[choice]
