import os
import bge
import mathutils

import defs
import json


def get_level_name(offset):
    level_dir = bge.logic.path('Levels')
    valid_dirs = list()
    for level in os.listdir(level_dir):
        dir_path = os.path.join(level_dir, level)
        if os.path.isdir(dir_path):
            if os.path.isfile(os.path.join(dir_path, defs.LEVEL_DATA_FILE)):
                valid_dirs.append(level)
    offset = mathutils.wrap(offset, 0, len(valid_dirs))
    return valid_dirs[offset]

def get_level_list():
    level_dir = bge.logic.path('Levels')
    valid_dirs = list()
    for level in os.listdir(level_dir):
        dir_path = os.path.join(level_dir, level)
        if os.path.isdir(dir_path):
            if os.path.isfile(os.path.join(dir_path, defs.LEVEL_DATA_FILE)):
                valid_dirs.append(dir_path)
    return valid_dirs


def get_level_data(offset):
    level_path = get_level_path(offset)
    data_file = open(os.path.join(level_path, defs.LEVEL_DATA_FILE))
    return json.load(data_file)

def get_level_path(offset):
    valid_dirs = get_level_list()
    choice = mathutils.wrap(offset, 0, len(valid_dirs))
    return valid_dirs[choice]
