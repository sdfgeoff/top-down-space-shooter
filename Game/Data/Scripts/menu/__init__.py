import defs
import bge

import loader
import events

from . import level
from . import ship
from . import options

def init(cont):
    '''Create the menu'''
    gamedata = bge.logic.globalDict.get('GAMEDATA', defs.DEFAULT_GAME)
    cont.owner['MASTER'] = Menu(cont.owner.scene, gamedata)
    cont.script = __name__ + '.update'

def update(cont):
    '''Update bge's scheduler'''
    cont.owner['MASTER'].update()


MODES = [
    {'name': 'Options Menu', 'obj': options.OptionsMenu},
    {'name': 'Level Picker', 'obj': level.LevelPicker},
    {'name': 'Ship Picker', 'obj': ship.ShipPicker}
]

class Menu():
    def __init__(self, scene, gamedata):
        bge.logic.globalDict['MENU'] = self
        bge.logic.addScene('Game')
        self.load_screen = loader.LoadScreen()
        self.loader = loader.Loader()
        self.load_screen.add_loader(self.loader)

        self.scheduler = bge.types.GX_Scheduler()
        self.gamedata = gamedata
        self.scene = scene
        self.modes = MODES.copy()
        self.loaded = False
        for mode in self.modes:
            mode['obj'] = mode['obj'](scene, gamedata, self)
        self.active_mode = 1

        self.active = True
        # Submenu's should use the menu's scheduler, so that the menu
        # can control if they are running or not
        mode_event = bge.types.GX_Event(self.update_mode)
        self.scheduler.register(mode_event)
        self.scheduler.register(bge.types.GX_Event(events.manager.update))

        # Ensure that the player's controls have been parsed into actual
        # event objects
        for player_id, player in enumerate(self.gamedata['players']):
            player['controls'] = events.load_config_dict(
                'player{}'.format(player_id), player['controls']
            )

        self.funct = self.load

    def load(self):
        '''Load the loader'''
        if not self.loader.done:
            self.loader.update()
        else:
            self.loaded = True
            self.funct = self.run

    def update_mode(self):
        '''Update the current mode'''
        self.funct()

    def update(self):
        '''update the menu scheduler'''
        if self.active:
            self.scheduler.update()


    def run(self):
        '''Handle changing between menu's using keyboard'''
        status_dict = dict()
        for player in self.gamedata['players']:
            controls = player['controls']
            for control in controls:
                if controls[control].state == bge.logic.KX_INPUT_JUST_ACTIVATED:
                    status_dict[control] = 1
        if 'gun' in status_dict:
            self.active_mode += 1
        if 'launcher' in status_dict:
            self.active_mode -= 1

    @property
    def active_mode(self):
        '''The active mode is what sub-menu the game is in.
        It needs a special setter so that it can disable all unactive
        sub-menu's'''
        return self._active_mode

    @active_mode.setter
    def active_mode(self, val):
        if val < 0:
            val = 0
        if val >= len(self.modes):
            bge.logic.globalDict['GAME'].start_game(self.gamedata)
            self.active = False
            self.scene.active_camera = self.scene.objects['Master']
        else:
            self._active_mode = val
            for mode in self.modes:
                mode['obj'].active = False
            self.modes[val]['obj'].active = True
