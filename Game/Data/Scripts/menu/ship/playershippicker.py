'''This contains the code for dealing with a single player's choice
of ship, and the interface for changing between them'''
import bge
import mathutils

import loader
import fx
import defs
import data

from . import shiptile

STATS_BLEND_PATH = bge.logic.path('Data', 'Menus', 'StatsTile.blend')


class PlayerShipPicker(object):
    '''This object is the entire ship picker for a single player'''
    _picture_path = ''
    _update_picture = False
    _selected = 0
    _mode = 0

    def __init__(self, owner, root_obj, playerdata):
        self.owner = owner
        self.playerdata = playerdata
        self.root_obj = root_obj

        # Schedule this object to update
        self.scheduler = owner.scheduler
        self.event = bge.types.GX_Event(self.update)
        self.scheduler.register(self.event)

        col = self.playerdata['color']
        self.color = mathutils.Vector([col['R'], col['G'], col['B'], 1])

        self.positions = [o for o in root_obj.groupMembers if 'SHIP_POS' in o]
        spawn = [o for o in root_obj.groupMembers if 'STATS' in o][0]
        self.stats_tile = StatsTile(self, spawn, self.color)

        self.tiles = list()
        for pos in self.positions:
            spawn_obj = pos
            ship_id = pos['SHIP_POS']
            self.tiles.append(shiptile.ShipTile(
                spawn_obj,
                ship_id,
                self.owner.shiptiles,
                self
            ))

        for tile in self.tiles:
            tile.object.color = self.color

        self.mode = defs.PLAYER_MODE_NONE
        self.selected = 0
        self.active = False

    def update(self):
        '''Looks at the players controls and uses them to change the
        current ship and mode'''
        if self.active:
            controls = self.playerdata['controls']
            status_dict = dict()
            for control in controls:
                if controls[control].state == bge.logic.KX_INPUT_JUST_ACTIVATED:
                    status_dict[control] = 1
                else:
                    status_dict[control] = 0

            if self.mode == defs.PLAYER_MODE_NONE:
                for control in status_dict:
                    if control != 'left' and status_dict[control] != 0:
                        self.mode = defs.PLAYER_MODE_PLAYER

            elif self.mode == defs.PLAYER_MODE_PLAYER:
                if status_dict['forwards']:
                    self.selected -= 1
                if status_dict['backwards']:
                    self.selected += 1
                if status_dict['left']:
                    self.mode = defs.PLAYER_MODE_NONE
                if status_dict['right']:
                    self.mode = defs.PLAYER_MODE_AI

            else:
                if status_dict['left']:
                    self.mode -= 1
                if status_dict['right'] and self.mode < len(defs.PLAYER_MODES) - 1:
                    self.mode += 1

        if self._update_picture:
            self._update_picture = False
            self.stats_tile.show_picture(self._picture_path)

    def set_picture(self, full_path):
        '''Sets the picture on the tile, caching it so it only changes
        once per frame'''
        self._update_picture = True
        self._picture_path = full_path

    def grey_out(self, state):
        '''Makes the tiles hide themselves slightly and bars/names
        dissapear'''
        for tile in self.tiles:
            if state:
                tile.object.color = self.color * 0.1
                self.name_text = ''
                self.stats(0, 0, 0)
            else:
                tile.object.color = self.color
                self.name_text = data.ship.get_ship_data(self._selected)['name']

    def stats(self, engine, gun, missile):
        '''Set the three stat bars'''
        mem = self.root_obj.groupMembers
        mem['EngineBar'].localScale.x = engine
        mem['EngineBar'].color = self.color
        mem['GunBar'].localScale.x = gun
        mem['GunBar'].color = self.color
        mem['MissileBar'].localScale.x = missile
        mem['MissileBar'].color = self.color

    @property
    def mode(self):
        '''Set if the player is a player or an AI'''
        return self._mode

    @mode.setter
    def mode(self, val):
        self._mode = val
        self.playerdata['mode'] = val
        mode_data = defs.PLAYER_MODES[self.mode]
        self.grey_out(mode_data['grey_out'])
        if mode_data['name'] is not None:
            self.name_text = mode_data['name']
        if mode_data['icon_overide'] is not None:
            full_path = bge.logic.path(
                'Data', 'Textures', mode_data['icon_overide']
            )
            self.set_picture(full_path)
        else:
            self.selected = self._selected  # Refresh the image etc.

    @property
    def selected(self):
        '''Set what vehicle is selected'''
        return self._selected

    @selected.setter
    def selected(self, val):
        self._selected = val
        ship_data = data.ship.get_ship_data(self._selected)
        if self.mode == defs.PLAYER_MODE_PLAYER:
            full_path = bge.logic.path(
                'Data', 'Models', 'Ships', ship_data['icon']
            )
            self.set_picture(full_path)
            self.name_text = ship_data['name']

            self.stats(
                get_engine_score(ship_data)*defs.BAR_ENGINE_SCALAR,
                get_gun_score(ship_data)*defs.BAR_GUN_SCALAR,
                get_launcher_score(ship_data)*defs.BAR_LAUNCHER_SCALAR
            )
        self.playerdata['ship'] = data.ship.get_ship_name(self._selected)
        for pos in self.positions:
            pos['ACTIVE_SHIP'] = pos['SHIP_POS'] + self.selected

    @property
    def name_text(self):
        '''Sets the large text used to display the name of the ship'''
        return self.root_obj.groupMembers['NameText'].text

    @name_text.setter
    def name_text(self, val):
        self.root_obj.groupMembers['NameText'].text = val


class StatsTile(object):
    '''The stats tile displays the statistics for a ship'''
    def __init__(self, owner, spawn_point, player_col):
        self.libname, objs = loader.lib_load_unique(STATS_BLEND_PATH)
        self.text_objs = dict()
        self.player_col = player_col
        for obj in objs:
            if obj.name == 'Background':
                self.object = obj
                obj.worldPosition = spawn_point.worldPosition
            if obj.name.startswith('Text'):
                added = obj.scene.addObject('GenericText', obj)
                added.worldTransform = obj.worldTransform
                self.text_objs[obj.name] = added
        for obj in self.object.childrenRecursive:
            obj.color = self.player_col

        self.event = bge.types.GX_Event(self.update)
        owner.scheduler.register(self.event)

    def update(self):
        '''Fades the tile to visible'''
        self.object.color = self.object.color.lerp(
            [1, 1, 1, 1],
            1-defs.LEVEL_TILE_SMOOTH
        )

    def show_picture(self, image_path):
        '''Sets the image on the tile, and sets the tile to
        black (it will automatically fade back in'''
        fx.tex.show_picture(self.object, image_path)
        self.object.color = [0, 0, 0, 1]


def get_engine_score(ship_data):
    '''Converts the properties of the engine to a single value'''
    force = ship_data['engine']['force']
    toque = ship_data['engine']['torque']
    energy = ship_data['engine']['energy_rate']
    return int(force * toque / energy / 10)


def get_gun_score(ship_data):
    '''Converts the properties of the gun to a single value'''
    delay = ship_data['gun']['refire_delay']
    damage = ship_data['gun']['bullet']['damage']
    timeout = ship_data['gun']['bullet']['timeout']
    speed = ship_data['gun']['bullet']['speed']
    energy = ship_data['gun']['energy_cost']

    shot_range = speed * timeout
    damage_per_second = damage / delay
    energy_per_second = energy / delay

    return int(shot_range * damage_per_second / energy_per_second / 10)


def get_launcher_score(ship_data):
    '''Converts the properties of the missile to a single value'''
    delay = ship_data['launcher']["refire_delay"]
    energy = ship_data['launcher']["energy_cost"]
    speed = ship_data['launcher']['missile']['speed']
    timeout = ship_data['launcher']['missile']['timeout']
    damage = ship_data['launcher']['missile']['damage']
    turn_rate = ship_data['launcher']['missile']['turn_rate']

    shot_range = speed * timeout
    damage_per_second = damage / delay
    energy_per_second = energy / delay
    manoverability = turn_rate / speed

    return int(shot_range * damage_per_second / energy_per_second * manoverability * 10)
