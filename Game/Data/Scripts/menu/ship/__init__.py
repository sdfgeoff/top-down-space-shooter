import defs
import bge

import loader
import events
import data

from . import playershippicker
from . import shiptile

def init(cont):
    if not cont.sensors[0].positive:
        return
    master = cont.owner.scene.objects.get('Master')
    gamedata = bge.logic.globalDict.get('GAMEDATA', defs.DEFAULT_GAME)
    bge.logic.globalDict['GAMEDATA'] = gamedata
    if not master:
        cont.owner['ShipPicker'] = ShipPicker(cont.owner.scene, gamedata)
        cont.owner['ShipPicker'].active = True
    cont.script = __name__ + ".run"

def run(cont):
    if not cont.sensors[0].positive:
        return
    if not 'Master' in cont.owner.scene.objects:
        cont.owner['ShipPicker'].scheduler.update()


class ShipPicker(object):
    def __init__(self, scene, gamedata, master=None):
        self.scene = scene
        self.gamedata = gamedata
        if master is None:
            loadscreen = loader.LoadScreen()
            self.loader = loader.Loader()
            loadscreen.add_loader(self.loader)
            self.funct = self.load
            self.scheduler = bge.types.GX_Scheduler()
            self.scheduler.register(bge.types.GX_Event(events.manager.update))
            for player_id, player in enumerate(self.gamedata['players']):
                player['controls'] = events.load_config_dict('player{}'.format(player_id), player['controls'])
        else:
            self.loader = master.loader
            self.funct = self.run
            self.scheduler = master.scheduler
        self.shiptiles = shiptile.ShipTiles(self.loader)

        self.players = list()
        for i in range(defs.MAX_PLAYERS):
            self.loader.register(self.create_player, args=[i])

        self.event = bge.types.GX_Event(self.update)
        self.active = False
        self.scheduler.register(self.event)



    def create_player(self, player_id):
        playerdata = self.gamedata['players'][player_id]
        root_obj = [o for o in self.scene.objects if o.get('PLAYER') == player_id][0]
        self.players.append(playershippicker.PlayerShipPicker(
            self,
            root_obj,
            playerdata
        ))
        if player_id <= 1:
            self.players[player_id].mode = defs.PLAYER_MODE_PLAYER

    def load(self):
        self.loader.update()
        if self.loader.done:
            self.funct = self.run

    def update(self):
        self.funct()

    def run(self):
        if self.active:
            self.scene.active_camera = self.scene.objects['ShipPickerCam']
        for player in self.players:
            player.active = self.active
