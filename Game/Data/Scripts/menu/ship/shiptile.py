'''The ship tiles each have an image of the ship on them. This file contains
two classes. One is for each ship tile, and the other is a load-helper that
makes sure that all the textures for the ships are pre-loaded into meshes.

I'm not too happy with the architecture of this, but don't have any better ideas
'''

import os

import bge
import mathutils

import defs
import data
import fx
import loader

TILE_PATH = bge.logic.path('Data', 'Menus', 'ShipTile.blend')


class ShipTile(object):
    '''The ship tile is a tile for each players flapwheel of ships.
    It can be set to display any ship's icon'''
    def __init__(self, spawn_obj, ship_id, tile_manager, master):
        self.root_obj = spawn_obj.groupObject
        self.object = tile_manager.create_tile(ship_id, spawn_obj)
        self._pos = ship_id
        self.tile_manager = tile_manager
        self.tile_manager.replace_mesh(self.object, ship_id)
        self.event = bge.types.GX_Event(self.update)
        master.scheduler.register(self.event)

    def update(self):
        '''Moves the ship tile to the correct location using linear
        interpolation. Automatically wraps around'''
        if 'init' not in self.object:
            self.pos = self._pos
            self.object['init'] = self

        targets = [o for o in self.root_obj.groupMembers if 'ACTIVE_SHIP' in o]
        target = [t for t in targets if t['ACTIVE_SHIP'] == self.pos]

        if not target:
            pos = self.pos - len(targets)
            target = [t for t in targets if t['ACTIVE_SHIP'] == pos]
            if not target:
                pos = self.pos + len(targets)
                target = [t for t in targets if t['ACTIVE_SHIP'] == pos]
            self.pos = pos
        current_pos = self.object.worldTransform
        target_pos = target[0].worldTransform
        self.object.worldTransform = target_pos.lerp(current_pos, defs.LEVEL_TILE_SMOOTH)

    @property
    def pos(self):
        '''The current position ID of the tile'''
        return self._pos

    @pos.setter
    def pos(self, val):
        '''Set what posistion this ship tile represents. Automatically
        updates the image on the tile'''
        self._pos = val
        self.tile_manager.replace_mesh(self.object, self.pos)


class ShipTiles(object):
    '''Load enough meshes that each ship icon has a mesh to be attached
    to. This also facilitates the creation of a ship tile and changing
    it's appearance to one of the other ship tiles'''
    def __init__(self, scene_loader):
        self.loader = scene_loader
        self.tiles = list()
        self.libnames = list()

        # register loading each tile to the loader so it can be done
        # asynchronusly
        for tile_id in range(len(data.ship.get_ship_list())):
            self.loader.register(self.load_ship, args=[tile_id])

    def load_ship(self, tile_id):
        '''Load a ship tile mesh and assign the correct picture to it'''
        # Load the shiptile blend
        libname, objs = loader.lib_load_unique(TILE_PATH)
        self.libnames.append(libname)

        # Assign the ship's image to the loaded mesh. Has to look up the
        # ship's data file to find the path and image name
        image_path = os.path.join(
            data.ship.get_ship_list()[tile_id],
            data.ship.get_ship_data(tile_id)['icon']
        )
        for obj in objs:
            if obj.name == 'Tile':
                self.tiles.append(obj)
                image_name = data.ship.get_ship_data(tile_id)['icon']
                image_path = bge.logic.path('Data', 'Models', 'Ships', image_name)
                fx.tex.show_picture(obj, image_path, 1, 0)

    def create_tile(self, ship_id, spawn_obj):
        '''Creates an instance of a ship tile at the specified object.'''
        ship_id = mathutils.wrap(ship_id, 0, len(self.tiles))
        return spawn_obj.scene.addObject(self.tiles[ship_id], spawn_obj)

    def replace_mesh(self, obj, new_id):
        '''Transmorgifies the object passed in to use one of the loaded
        in ship tile meshes'''
        new_id = mathutils.wrap(new_id, 0, len(self.tiles))
        new_mesh = self.tiles[new_id].meshes[0]
        obj.replaceMesh(new_mesh)
