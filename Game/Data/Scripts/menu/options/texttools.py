import bge
import mathutils


class Text(object):
    '''We want to be able to click on individual characters in a text object.
    So this spawns a physics mesh that represents each character. This can be
    detected with a raycast. You can bind a function to a character using
    register_hover_callback'''
    def __init__(self, scene, spawn_obj, fontspec=None):
        self.scene = scene
        register_text_hoverer(scene)

        self.text_obj = self.scene.addObject('TEXT', spawn_obj)
        self.hoverpoints = list()
        self.fontspec = fontspec

        self._text = ''
        self.text = ''
        self.update_hoverpoints()
        self.character_callbacks = {}

    def update_hoverpoints(self):
        '''Places objects so that physics engine can detect what character is
        hovered on by casting a ray'''
        while len(self.text) > len(self.hoverpoints):
            new = self.scene.addObject('CLICKHOTZONE', self.text_obj)
            new['TEXT_OBJECT'] = self
            self.hoverpoints.append(new)
            new.setParent(self.text_obj)

        prev_x = 0
        prev_y = 0
        for char_id, char in enumerate(self.text):
            # Move the objects to the correct place
            point = self.hoverpoints[char_id]
            point.localPosition.x = prev_x
            point.localPosition.y = prev_y
            point.visible = False

            # Get ready for the next character
            y_offset, reset = self.get_char_y_offset(char)
            prev_y -= y_offset
            if not reset:
                # Do a normal offset on the x axis
                x_width = self.get_char_x_offset(char)
                point.localScale.x = x_width
                prev_x += x_width
            else:
                # We hit a newline, and need to reset back to 0
                prev_x = 0.0
                point.localScale.x = 0.0

    def hovered_on(self, hoverpoint):
        '''Run this whenever you want to hover on a hoverpoint'''
        try:
            char_id = self.hoverpoints.index(hoverpoint)
        except ValueError:
            char_id = None
        if char_id in self.character_callbacks:
            self.character_callbacks[char_id](self, char_id)

    def register_hover_callback(self, char_id, function):
        '''Sets a character so that it runs a function when it is hovered on'''
        self.character_callbacks[char_id] = function

    def get_char_x_offset(self, char):
        '''Returns  how much to offset the next hoverpoint by horizontally.
        This is the characters width. This is font dependant'''
        if self.fontspec is None:
            return 0.16  # @todo: Parse the font somehow?
        return self.fontspec[char][0]

    def get_char_y_offset(self, char):
        '''Retuns how much to offset the next character by vertically. This is
        zero except for newlines.
        The second return argument is if it should zero the x offset'''
        if self.fontspec is None:
            if char == '\n':
                return 0.27, True  # @todo: Parse the font somehow?
            return 0.0, False
        else:
            return self.fontspec[char][1]

    def highlight_char(self, char_id, state=True):
        '''Sets the background of a character to visible'''
        self.hoverpoints[char_id].visible = state

    @property
    def text(self):
        '''What is currently stored in the text object'''
        return self._text

    @text.setter
    def text(self, val):
        self._text = val
        self.text_obj.text = val
        self.update_hoverpoints()


KNOWN_SCENES = dict()


def register_text_hoverer(scene):
    '''Makes sure each scene has a function that checks for the hovering of
    text objects'''
    if scene not in KNOWN_SCENES:
        KNOWN_SCENES[scene] = dict()
        scene.pre_draw.append(text_object_hoverer)


def text_object_hoverer():
    '''Checks for text objects that are being hovered on'''
    scene = bge.logic.getCurrentScene()
    mouse_over = cast_mouse_ray(scene)
    if mouse_over is not None:
        if 'TEXT_OBJECT' in mouse_over:
            mouse_over['TEXT_OBJECT'].hovered_on(mouse_over)


def cast_mouse_ray(scene):
    '''Returns the object that is hovered on'''
    active_camera = scene.active_camera
    hit_x, hit_y = bge.logic.mouse.position
    vect = active_camera.getScreenVect(hit_x, hit_y)
    pos = active_camera.worldPosition.copy()
    if not active_camera.perspective:
        width = active_camera.ortho_scale
        aspect = bge.render.getWindowHeight() / bge.render.getWindowWidth()
        height = width * aspect
        offset = mathutils.Vector([
            (hit_x - 0.5) * width,
            -(hit_y - 0.5) * height,
            0
        ])
        pos += active_camera.getAxisVect(offset)
        vect = active_camera.getAxisVect([0, 0, 1])
    obj, _hit_pos, _ = active_camera.rayCast(
        pos - vect,
        pos,
        500, '', 1, True
    )
    return obj
