import defs
import bge
import mathutils

import loader
import events

from . import texttools


def init(cont):
    '''Creates the options scene'''
    if not cont.sensors[0].positive:
        return
    master = cont.owner.scene.objects.get('Master')
    gamedata = bge.logic.globalDict.get('GAMEDATA', defs.DEFAULT_GAME)
    bge.logic.globalDict['GAMEDATA'] = gamedata
    if not master:
        cont.owner['Options'] = Options(cont.owner.scene, gamedata)
        cont.owner['Options'].active = True
    cont.script = __name__ + ".run"


def run(cont):
    '''Updates the options scene'''
    if not cont.sensors[0].positive:
        return
    if 'Master' not in cont.owner.scene.objects:
        cont.owner['Options'].scheduler.update()


class OptionsMenu(object):
    '''The options scene overall'''
    def __init__(self, scene, gamedata, master=None):
        self.scene = scene
        self.gamedata = gamedata
        self.box = None
        if master is None:
            loadscreen = loader.LoadScreen()
            self.loader = loader.Loader()
            loadscreen.add_loader(self.loader)
            self.scheduler = bge.types.GX_Scheduler()
            self.scheduler.register(bge.types.GX_Event(events.manager.update))
        else:
            self.loader = master.loader
            self.scheduler = master.scheduler
        self.funct = self.load
        self._active = False
        self.active = False
        self.game_options_text = GameOptions(self.scene.objects['GAME_OPTIONS'])
        self.game_options_text.onselect.append(self.edit_setting)

        self.event = bge.types.GX_Event(self.update)
        self.scheduler.register(self.event)

    def edit_setting(self, setting_info):
        setting_path = setting_info['setting']
        val = get_setting_val(setting_path)
        if isinstance(val, bool):
            set_setting_val(setting_path, not val)
            self.game_options_text.update_text()
        else:
            self.game_options_text.active = False
            self.box = BindingBox(self.scene.objects['BINDING_BOX'], setting_info)
            self.box.ondone.append(self.resume_editing)

    def resume_editing(self, _box):
        self.game_options_text.active = True
        self.box = None
        self.game_options_text.update_text()

    @property
    def active(self):
        return self._active

    @active.setter
    def active(self, val):
        self._active = val
        if val is True:
            self.scene.active_camera = self.scene.objects['OptionsCamera']

    def run(self):
        '''Updates the options scene'''
        if self.active:
            if self.box:
                self.box.update()

    def load(self):
        '''Loads the options scene'''
        self.loader.update()
        if self.loader.done:
            self.funct = self.run

    def update(self):
        '''Updates or loads the options scene'''
        self.funct()


SETTING_TO_NAME = {
    'forwards':'FAST',
    'backwards':'SLOW',
    'left':'LEFT',
    'right':'RIGHT',
    'gun':'BULLET',
    'launcher':'MISSILE'
}

NEWLINE = '\n'
KEY_TEXT = {'name':7, 'setting':1}
KEY_VALUE = {'name':1, 'setting':7}
BOOL =  {'name':12, 'setting':1}
BOTTOM_INDENT = {'name': '     '}

GAME_OPTIONS = [
    {'name': '  '},
    {'name': SETTING_TO_NAME['forwards'], 'display': KEY_TEXT},
    {'name': SETTING_TO_NAME['backwards'], 'display': KEY_TEXT},
    {'name': SETTING_TO_NAME['left'], 'display': KEY_TEXT},
    {'name': SETTING_TO_NAME['right'], 'display': KEY_TEXT},
    {'name': SETTING_TO_NAME['gun'], 'display': KEY_TEXT},
    {'name': SETTING_TO_NAME['launcher'], 'display': KEY_TEXT},

    {'name': '\n1 '},
    {'setting': ['players', 0, 'controls', 'forwards'], 'display': KEY_VALUE, 'select': True},
    {'setting': ['players', 0, 'controls', 'backwards'], 'display': KEY_VALUE, 'select': True},
    {'setting': ['players', 0, 'controls', 'left'], 'display': KEY_VALUE, 'select': True},
    {'setting': ['players', 0, 'controls', 'right'], 'display': KEY_VALUE, 'select': True},
    {'setting': ['players', 0, 'controls', 'gun'], 'display': KEY_VALUE, 'select': True},
    {'setting': ['players', 0, 'controls', 'launcher'], 'display': KEY_VALUE, 'select': True},
    {'name': '\n2 '},
    {'setting': ['players', 1, 'controls', 'forwards'], 'display': KEY_VALUE, 'select': True},
    {'setting': ['players', 1, 'controls', 'backwards'], 'display': KEY_VALUE, 'select': True},
    {'setting': ['players', 1, 'controls', 'left'], 'display': KEY_VALUE, 'select': True},
    {'setting': ['players', 1, 'controls', 'right'], 'display': KEY_VALUE, 'select': True},
    {'setting': ['players', 1, 'controls', 'gun'], 'display': KEY_VALUE, 'select': True},
    {'setting': ['players', 1, 'controls', 'launcher'], 'display': KEY_VALUE, 'select': True},
    {'name': '\n3 '},
    {'setting': ['players', 2, 'controls', 'forwards'], 'display': KEY_VALUE, 'select': True},
    {'setting': ['players', 2, 'controls', 'backwards'], 'display': KEY_VALUE, 'select': True},
    {'setting': ['players', 2, 'controls', 'left'], 'display': KEY_VALUE, 'select': True},
    {'setting': ['players', 2, 'controls', 'right'], 'display': KEY_VALUE, 'select': True},
    {'setting': ['players', 2, 'controls', 'gun'], 'display': KEY_VALUE, 'select': True},
    {'setting': ['players', 2, 'controls', 'launcher'], 'display': KEY_VALUE, 'select': True},
    {'name': '\n4 '},
    {'setting': ['players', 3, 'controls', 'forwards'], 'display': KEY_VALUE, 'select': True},
    {'setting': ['players', 3, 'controls', 'backwards'], 'display': KEY_VALUE, 'select': True},
    {'setting': ['players', 3, 'controls', 'left'], 'display': KEY_VALUE, 'select': True},
    {'setting': ['players', 3, 'controls', 'right'], 'display': KEY_VALUE, 'select': True},
    {'setting': ['players', 3, 'controls', 'gun'], 'display': KEY_VALUE, 'select': True},
    {'setting': ['players', 3, 'controls', 'launcher'], 'display': KEY_VALUE, 'select': True},

    {'name': '\n'},
    {'name': '\n'},BOTTOM_INDENT,
    {'name': 'FULLSCREEN', 'setting': ['game', 'fullscreen'], 'display': BOOL, 'select': True},
    BOTTOM_INDENT,{'name': 'MUSIC', 'setting': ['game', 'music'], 'display': BOOL, 'select': True},
    {'name': '\n'},BOTTOM_INDENT,
    {'name': 'BLOOM', 'setting': ['game', 'bloom'], 'display': BOOL, 'select': True},
    BOTTOM_INDENT,{'name': 'SOUND', 'setting': ['game', 'sound'], 'display': BOOL, 'select': True},
    {'name': '\n'},BOTTOM_INDENT,
    {'name': 'TRAILS', 'setting': ['game', 'trails'], 'display': BOOL, 'select': True},
]


class GameOptions(object):
    def __init__(self, spawn_obj):
        self.scene = spawn_obj.scene
        self.text_obj = texttools.Text(self.scene, spawn_obj)
        self.click_zones = dict()
        self.update_text()
        self.debounce = False
        self.active = True

        self.onselect = list()

    def update_text(self):
        count = 0
        self.text_obj.text = ''
        self.click_zones = dict()
        for option in GAME_OPTIONS:

            display_size = option.get('display', {'name': 1, 'setting': 1})
            name = option.get('name', None)
            setting = option.get('setting', None)

            out_str = ''
            if name is not None:
                format_str = "{:" + str(display_size['name']) + "}"
                out_str = format_str.format(name)
            if setting is not None:
                format_str = "{:" + str(display_size['setting']) + "}"
                out_str += format_str.format(get_setting_string(setting))

            self.text_obj.text += out_str
            for _ in enumerate(out_str):
                self.text_obj.register_hover_callback(count, self.hover_over)
                self.click_zones[count] = option
                count += 1

    def hover_over(self, text_obj, char_id):
        '''Runs whenever a character is hovered over'''
        if self.active:
            for zone in self.click_zones:
                if self.click_zones[zone] is self.click_zones[char_id]:
                    if self.click_zones[zone].get('select', False):
                        text_obj.highlight_char(zone, True)
                else:
                    text_obj.highlight_char(zone, False)

            if bge.logic.mouse.events[bge.events.LEFTMOUSE]:
                if not self.debounce:
                    self.debounce = True
                    for funct in self.onselect:
                        funct(self.click_zones[char_id])
            else:
                self.debounce = False


class BindingBox:
    def __init__(self, source_obj, setting_data):
        self.obj = source_obj.scene.addObject('EditBindingBox', source_obj)
        self.ondone = list()
        self.setting_data = setting_data

        self.obj.children['EditBindingTitle'].text = "Player {} {}".format(
            setting_data['setting'][1] + 1,
            SETTING_TO_NAME[setting_data['setting'][3]]
        )

    def update(self):
        for source in events.get_active_sources():
            if isinstance(source, events.sources.KeyButtonSource):
                if source.key == 'ESCKEY':
                    self.end()
                    break
            if not isinstance(source, events.sources.MouseButtonSource):
                self.set_event(source)
            break

    def set_event(self, event):
        set_setting_val(self.setting_data['setting'], events.inputfilter.InputFilter(event))
        self.end()


    def end(self):
        self.obj.endObject()
        for funct in self.ondone:
            funct(self)






def get_setting_val(setting_path):
    val = defs.DEFAULT_GAME
    for path in setting_path:
        val = val[path]
    return val


def get_setting_string(setting_path):
    val = get_setting_val(setting_path)
    if isinstance(val, bool):
        if val:
            return 'Y'
        else:
            return 'N'

    e = None
    if isinstance(val, dict):
        e = events.inputfilter.from_dict(val)
    elif isinstance(val, events.inputfilter.InputFilter):
        e = val

    if e is not None:
        return e.source.short_description()

    else:
        return val


def set_setting_val(setting_path, new_val):
    val = defs.DEFAULT_GAME
    for path in setting_path[:-1]:
        val = val[path]
    val[setting_path[-1]] = new_val
    defs.DEFAULT_GAME.save()
