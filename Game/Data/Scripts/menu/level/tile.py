import os
import bge

import defs
import fx
import loader
import data


# Path to the blend containing the tile used to display the level preview on
TILE_PATH = bge.logic.path('Data', 'Menus', 'LevelTile.blend')


class Tile(object):
    '''This displays a preview for a level'''
    _pos = 0
    def __init__(self, spawn_obj, pos, master):
        self.scene = spawn_obj.scene
        self._pos = pos

        # Create a tile entity to display on
        self.object = add_tile()

        # Get the object to update
        self.event = bge.types.GX_Event(self.update)
        master.scheduler.register(self.event)

    def update(self):
        '''Moves the tile towards it's correct position and makes sure it's
        displaying the correct thing'''
        # We can't set pos on the first frame because showing the picture
        # won't work (arrgh). So we have to init the object the hard way
        if 'init' not in self.object:
            self.pos = self._pos
            self.object['init'] = self

        # Find the object the level picker tile is currently associated with.
        # Note that the object will change as the player pushes buttons
        # and the level ID associated with each position changes.
        targets = [o for o in self.scene.objects if 'ACTIVE' in o]
        target = [t for t in targets if t['ACTIVE'] == self.pos]

        # If the target position does not exist, wrap in both directions to
        # try and find one. There will be a better way than this, but I'm
        # lazy and this works.
        if not target:
            pos = self.pos - len(targets)
            targets = [o for o in self.scene.objects if 'ACTIVE' in o]
            target = [t for t in targets if t['ACTIVE'] == pos]
            if not target:
                pos = self.pos + len(targets)
                targets = [o for o in self.scene.objects if 'ACTIVE' in o]
                target = [t for t in targets if t['ACTIVE'] == pos]
            self.pos = pos

        # Lerp the tile towards the target position
        current_pos = self.object.worldTransform
        target_pos = target[0].worldTransform
        self.object.worldTransform = target_pos.lerp(
            current_pos,
            defs.LEVEL_TILE_SMOOTH
        )

    @property
    def pos(self):
        '''Get's the position in the level list that the tile is
        currently displaying.'''
        return self._pos

    @pos.setter
    def pos(self, val):
        show_picture(self.object, val)
        self._pos = val


def show_picture(obj, offset):
    '''Shows the picture associated with the specified level on the specified
    object. It has to load it from disk, so this is quite slow'''
    # Extract path from level data file
    data_file = data.level.get_level_data(offset)
    image_path = os.path.join(data.level.get_level_path(offset), data_file['icon'])

    # Update the level name on the tile
    # @todo: should this be here?
    obj.children['LevelNameText'].text = str(data_file['name'])

    # Update the objects texture
    fx.tex.show_picture(obj, image_path, 0, 0)


def add_tile():
    '''Adds a tile from the tile blend. Because text objects don't load, it
    also replaces the text object with one added from a hidden layer in the
    level picker blend'''
    _lib_name, objs = loader.lib_load_unique(TILE_PATH)
    # Sort through the loaded objects and look for the:
    #  - tile object that everything is parented to
    #  - text object location it can be replaced with one that will work.
    for obj in objs:
        if obj.name == 'Tile':
            root = obj
        if obj.name == 'NameTextLoc':
            # Text object's don't libload, so add one from a hidden layer
            text_obj = obj.scene.addObject('LevelNameText', obj)
            text_obj.setParent(root)
            text_obj.resolution += 3
    return root
