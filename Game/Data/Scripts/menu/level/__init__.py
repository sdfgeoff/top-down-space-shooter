import bge
import mathutils

import defs
from .tile import Tile
import data
import loader
import events


def init(cont):
    if not cont.sensors[0].positive:
        return
    gamedata = bge.logic.globalDict.get('GAMEDATA', defs.DEFAULT_GAME)
    bge.logic.globalDict['GAMEDATA'] = gamedata
    master = cont.owner.scene.objects.get('Master')
    if not master:
        cont.owner['LevelPicker'] = LevelPicker(cont.owner.scene, gamedata)
        cont.owner['LevelPicker'].active = True
    cont.script = __name__ + ".run"

def run(cont):
    if not cont.sensors[0].positive:
        return
    if not 'Master' in cont.owner.scene.objects:
        cont.owner['LevelPicker'].scheduler.update()

class LevelPicker(object):
    def __init__(self, scene, gamedata, master=None):
        self.gamedata = gamedata
        self.scene = scene
        self.tile_locations = [o for o in scene.objects if 'POS' in o]
        self.tiles = list()
        self.event = bge.types.GX_Event(self.update)

        if master is None:
            loadscreen = loader.LoadScreen()
            self.loader = loader.Loader()
            loadscreen.add_loader(self.loader)
            self.funct = self.load
            self.scheduler = bge.types.GX_Scheduler()
            self.scheduler.register(bge.types.GX_Event(events.manager.update))
            for player_id, player in enumerate(self.gamedata['players']):
                player['controls'] = events.load_config_dict('player{}'.format(player_id), player['controls'])
        else:
            self.loader = master.loader
            self.funct = self.run
            self.scheduler = master.scheduler
        self.scheduler.register(self.event)

        self.tiles = list()
        for tile in self.tile_locations:
            self.loader.register(self.add_tile, args=[tile])


        self.active = False
        self.selected = 0


    def add_tile(self, tile):
        tile['ACTIVE'] = tile['POS']
        self.tiles.append(Tile(tile, tile['POS'], self))

    def load(self):
        if self.active:
            self.loader.update()
            if self.loader.done:
                self.funct = self.run

    def update(self):
        self.funct()

    def run(self):
        if self.active:
            status_dict = dict()
            events = bge.logic.keyboard.events
            for p in self.gamedata['players']:
                controls = p['controls']
                for c in controls:
                    if controls[c].state == bge.logic.KX_INPUT_JUST_ACTIVATED:
                        status_dict[c] = 1
            if 'left' in status_dict:
                self.selected -= 1
            if 'right' in status_dict:
                self.selected += 1


            if bge.logic.mouse.events[bge.events.LEFTMOUSE] == bge.logic.KX_INPUT_JUST_ACTIVATED:
                mouse_over = cast_mouse_ray(self.scene)
                if mouse_over is not None:
                    self.selected += mouse_over.get('CHANGE_LEVEL', 0)

    @property
    def selected(self):
        return self._selected

    @selected.setter
    def selected(self, val):
        self._selected = val
        level = data.level.get_level_name(val)
        self.gamedata['level'] = level
        for tile in self.tile_locations:
            tile['ACTIVE'] = tile['POS'] + self.selected


    @property
    def active(self):
        return self._active

    @active.setter
    def active(self, val):
        self._active = val
        if val is True:
            self.scene.active_camera = self.scene.objects['LevelPickerCam']


def cast_mouse_ray(scene):
    active_camera = scene.active_camera
    hit_x, hit_y = bge.logic.mouse.position
    vect = active_camera.getScreenVect(hit_x, hit_y)
    pos = active_camera.worldPosition.copy()
    if not active_camera.perspective:
        width = active_camera.ortho_scale
        height = width / bge.render.getWindowWidth() * bge.render.getWindowHeight()
        offset = mathutils.Vector([
            (hit_x - 0.5) * width,
            -(hit_y - 0.5) * height,
            0
        ])
        pos += active_camera.getAxisVect(offset)
        vect = active_camera.getAxisVect([0, 0, 1])
    obj, hit_pos, _ = active_camera.rayCast(
        pos - vect,
        pos,
        500, '', 1, True
    )
    return obj
