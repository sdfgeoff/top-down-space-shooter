'''This module contains FX classes associated with altering meshes'''
import bgeextensions
import bge

import mathutils

class Trail(object):
    '''Makes a mesh trail behind an object. Pass in a scheduler that
    will run the update attribute every timestep'''
    def __init__(self, obj, scheduler=None):
        self.obj = obj

        # Because each trail needs to follow a different course, it
        # needs a unique mesh. Thankfully LibNew can do this (despite
        # it's other limitations).
        self._libname = self._generate_lib_name()
        mesh_name = obj.meshes[0].name
        new_mesh = bge.logic.LibNew(self._libname, 'Mesh', [mesh_name])[0]
        self.obj.replaceMesh(new_mesh)

        # We need to de-parent the trail to prevent one-frame glitches
        # when the object rotates or translates quickly
        self._follow_obj = self.obj.parent
        self._offset = self.obj.localTransform.copy()
        self.obj.removeParent()

        # Store all the verts (and their offsets) in order so we don't
        # have to generate this each frame.
        self._verts = self._get_verts()

        # A length scalar so the trail can be made longer without
        # needing more vertices
        self.length = 1.0

        # The pos_list stores the worldTransforms of all the places the
        # parent object has been. These are used to place the vertices
        self.pos_list = list()

        # Schedule the trail to be updated. Unfortunately this is
        # a relatively processor intesive task. I think there should
        # be some way to speed this up, but I am not sure of it yet.
        if scheduler is not None:
            self._event = bge.types.GX_Event(self.update)
            self._scheduler = scheduler
            scheduler.register(self._event)

        #Ensure the trail has no current postions
        self.reset()

    def _generate_lib_name(self):
        '''Generates a name in which to store the trail mesh in liblist

        This is done by starting with the mesh name, checking if the
        name is already in the list, incrementing a number if it is
        '''
        mesh_name = self.obj.meshes[0].name
        i = 0
        while mesh_name + str(i) in bge.logic.LibList():
            i += 1
        return mesh_name + str(i)


    def _get_verts(self):
        '''Returns a list of verts that is ordered from one to the
        other (verts are the same position if within 3dp).

        Each entry is a list of verts at that position:
        [
         closest_to_follow_obj
         ((vert, x_offset, z_offset), (vert, x_offset, z_offset)),
         ((vert, x_offset, z_offset), (vert, x_offset, z_offset)),
         ((vert, x_offset, z_offset), (vert, x_offset, z_offset)),
         furthest_from_follow_obj
        ]'''
        mesh = self.obj.meshes[0]

        # To figure out vertex order properly, they are put into a
        # dictionary with the rounded x location as a key. Then
        # the keys can be sorted and a list of ordered vertices
        # created
        vert_dict = {}
        for vert_id in range(mesh.getVertexArrayLength(0)):
            vert = mesh.getVertex(0, vert_id)
            key = int(vert.y*100)  # So we don't have FP issues with rounding
            vert_info = (vert, vert.x, vert.z)
            vert_dict[key] = vert_dict.get(key, list())
            vert_dict[key].append(vert_info)

        # Creating the ordered list
        out_list = []
        for key in sorted(list(vert_dict.keys()), reverse=False):
            out_list.append(vert_dict[key])
        return out_list

    def update(self):
        '''Updates the position of the vertices'''
        follow = self._follow_obj
        pos_list = self.pos_list
        num_req = int((len(self._verts) - 1) * self.length)

        # Add the new transformation on to the list of positions and
        # move the trail object to the proper place.
        new_trans = follow.worldTransform * self._offset
        self.obj.worldTransform = new_trans
        pos_list.append(new_trans)

        if len(pos_list) > num_req:
            # Clamp the length of the list. Is there a faster way than
            # this? I suspect this is taking a fairly long time as
            # the list is quite long and it has to be re-allocated etc.
            self.pos_list = pos_list[-num_req:]
        elif len(pos_list) == 2:
            # For some reason, worldTransform doesn't update properly,
            # so if you set worldTransform and reset the trail at the
            # same time, it won't start from the new position.
            # To remedy this, we simply make sure that when the trail is
            # two units long we set both first and second positions to
            # the new one.
            pos_list[0] = pos_list[1]

        # When the trail first spawns, we want it to all be in existence
        # so we need to offset the lookups
        pos_offset = len(pos_list) - num_req

        #So we can find the local co-ordinates for the vertices
        inverted_trans = new_trans.inverted()

        for pos_id, vert_pos in enumerate(self._verts):
            # Extract what transformation the vertex has by looking it
            # up in the position list.
            pos_id = int(pos_id * self.length)
            pos_id = pos_id + pos_offset
            trans = pos_list[min(len(pos_list) - 1, max(pos_id, 0))]

            # Iterate through all the vertices associated with this
            # position and move htem to the correct place
            for vert, x_offset, z_offset in vert_pos:
                vert_offset = mathutils.Vector([x_offset, 0, z_offset])
                offset = trans * vert_offset  # into worldspace
                out = inverted_trans * offset  # into localspace
                vert.setXYZ(out)

    def reset(self):
        '''Clears the position list'''
        self.pos_list = list()

    def end(self):
        '''Removes all game-specific data associated with this object'''
        self._scheduler.remove(self._event)
        bge.logic.LibFree(self._libname)


def set_vert_col(obj, col):
    '''Sets all vertices in the mesh to a specific color. The color
    should be a four long iterable of form RGBA'''
    mesh = obj.meshes[0]
    for vert_id in range(mesh.getVertexArrayLength(0)):
        vert = mesh.getVertex(0, vert_id)
        vert.setRGBA(col)
