'''The fx module contains scripts used to create graphical effects.'''
import math
import bge
import mathutils

from .mesh import Trail
from . import tex

def brownian(cont):
    '''Applies random motion to global x and y. Control using
    a game property 'BROWNIAN' to set the amount'''
    amt = cont.owner['BROWNIAN']
    cont.owner.worldPosition.x += (bge.logic.getRandomFloat() - 0.5) * amt
    cont.owner.worldPosition.y += (bge.logic.getRandomFloat() - 0.5) * amt
    cont.owner.applyRotation([0, 0, (bge.logic.getRandomFloat() - 0.5) * amt * math.pi])


def trail(cont):
    '''Turns this objects mesh into a trail for it's parent. If the trail
    already exists, it updates the trail length from the trail_length
    property. Make sure the bge.logic.scheduler is being run'''
    if 'TRAIL' not in cont.owner:
        cont.owner['TRAIL'] = Trail(cont.owner, bge.logic.scheduler)

    cont.owner['TRAIL'].length = max(0.0, cont.owner.get('TRAIL_LENGTH', 1.0))


def increment_ob_col(cont):
    '''Increments an object color by an amount specified in game engine'''
    # Get the amount for each channel
    red = cont.owner.get('INC_R', 0.0)
    green = cont.owner.get('INC_G', 0.0)
    blue = cont.owner.get('INC_B', 0.0)

    # Increment and apply
    col = cont.owner.color
    col[0] += red
    col[1] += green
    col[2] += blue
    cont.owner.color = col

def update(cont):
    '''Cheapo way to update the scheduler'''
    bge.logic.scheduler.update()
