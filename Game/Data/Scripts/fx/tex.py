'''Various texture effects. NOTE THAT THIS SHOULD BE MOVED INTO THE FX PACKAGE
AT SOME POINT IN THE NEAR FUTURE'''

import os
import bge


def show_picture(obj, path, mat_id=0, tex_id=0):
    '''Loads a picture into a texture'''
    # The texture has to be stored in a place associated with other game data
    # so we store it in a game property. This name inlucdes the mat ID's and
    # tex ID's so that a single object with a complex setup does not have the
    # textures overwrite each other.
    # If a bge.texture object already exists for this object/mat_id/tex_id, then
    # we retrieve that.
    prop_name = 'SHOW_PICTURE{}:{}'.format(mat_id, tex_id)
    if prop_name not in obj:
        tex = bge.texture.Texture(obj, mat_id, tex_id)
        obj[prop_name] = tex
    else:
        tex = obj[prop_name]

    # Load the image from the path
    raw = bge.texture.ImageFFmpeg(os.path.join(path))

    # Check to see that it loaded
    if raw.status == bge.texture.SOURCE_ERROR:
        # Error in loading image
        bge.log.error("Unable to load image at {}".format(path))
        raise ValueError("Unable to load image at {}".format(path))

    # Assign the new image to the texture and update the texture.
    tex.source = raw
    tex.refresh(True)
