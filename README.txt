Fly your spacecraft in top-down combat against a human or AI opponents.

In 1993, a DOS game called splaymaster was released. It allowed players 
to enter a top-down space duel against either another player (sitting 
at the same computer), or an AI opponent. It is a great party game, as 
the controls are easy to pick up, and the whole game can be explained 
in about 30 seconds.

This game aims to take the core of the game experience and bring it to 
a modern audience. Click image for larger version. 

== Features
✓ Choose from a range of ships
✓ Choose from a range of levels
✓ Up to four players on the same computer
- AI opponents
- Game modes including ("First to Four", "Arena King", and "Three Minute Massacre")
- Accepts keyboard and joystick controls

This game will be released under the GPL license, free for people to 
edit, redistribute and do what they want with.

The purpose of this game is to do a high-quality, completed game in a 
three month development schedule. The first two months will be spent 
developing the gameplay and the assets. During this time, screenshots 
and videos may be released, but the actual game will not be available 
until a beta period in April. The aim of the beta is to get user 
feedback on the game, and to balance the player ships.
